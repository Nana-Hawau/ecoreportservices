package com.ecobank.ecoreportservices.constants;

public enum CardsClusters {
    ENG ("Nigeria", "ENG"),
    CESA1 ("Central & East Africa 1", "CESA1"),
    CESA2 ("Central & East Africa 2", "CESA2"),
    AWA ("Anglophone West Africa", "AWA"),
    FWA ("Francophone West Africa", "FWA");

    private final String subServiceName;
    private final String subServiceCode;

    CardsClusters (String subServiceName, String subServiceCode) {
        this.subServiceName = subServiceName;
        this.subServiceCode = subServiceCode;
    }



    public String getSubServiceName() {
        return subServiceName;
    }

    public String getSubServiceCode() {
        return subServiceCode;
    }

    public static boolean contains(String test) {
        for (CardsClusters cardsClusters : CardsClusters.values()) {
            if (cardsClusters.getSubServiceCode().equals(test)) {
                return true;
            }
        }
        return false;
    }

    public static String getEnumByString(String subServiceCode){
        for(CardsClusters c : CardsClusters.values()){
            if(c.subServiceCode.equals(subServiceCode)) return c.name();
        }
        return null;
    }
}
