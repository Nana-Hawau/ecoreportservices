package com.ecobank.ecoreportservices.constants;

public class MethodConstants {
    public static final String CARDS = "cardServicesController.getSearchDataCards";
    public static final String INTER_AFF_METHOD_NAME = "getSearchTransDataInterAff";
    public static final String INTL_TRANSFER_METHOD_NAME= "getSearchTransDataInternationalTrans";
    public static final String BILLS_PAYMENT_METHOD_NAME = "getSearchDataBillPayments";
    public static final String INTER_BANK_METHOD_NAME = "getSearchDataInterBankTransfer";


}
