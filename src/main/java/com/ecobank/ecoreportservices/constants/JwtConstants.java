package com.ecobank.ecoreportservices.constants;

public class JwtConstants {
    public static final String SECRET = "SecretKeyToGenJWTs";
    public static final String ISSUER = "Ecobank";
    public static final long EXPIRATION_TIME = 3_600_000; // 1hr
    public static final String HEADER_STRING = "Authorization";
    public static final String TOKEN_PREFIX = "Bearer ";
}
