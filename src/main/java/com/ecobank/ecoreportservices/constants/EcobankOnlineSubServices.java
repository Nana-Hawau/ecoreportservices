package com.ecobank.ecoreportservices.constants;

public enum EcobankOnlineSubServices {
    processInternationalTransfer("International Transfer", "INTLTRF"),
    InternationalSwiftTransfer("International Swift Transfer", "INTLSTRAF"),
    processDomesticTransfer("Domestic Transfer", "DOMTRF"),
    BILL("Bills Payment Across Group", "BILLSGROUP"),
    processSelfTransfer("Self Transfer", "STRF"),
    NIP_PAY("NIBSS Payment", "NIBPAY"),
    RTSendMoney("Rapid Transfer", "RT"),
    processInternalTransfer("Internal Transfer", "INTTRF"),
    BILL_PAY("Bills Payments", "BILLSPAY");

    private String getSubServiceName;
    private String subServiceCode;

    EcobankOnlineSubServices(String getSubServiceName, String subServiceCode) {
        this.getSubServiceName = getSubServiceName;
        this.subServiceCode = subServiceCode;
    }



    public String getSubServiceName () {
        return getSubServiceName;
    }

    public String getSubServiceCode() {
        return subServiceCode;
    }

    public static boolean contains(String test) {

        for (EcobankOnlineSubServices ecobankOnlineSubServices : EcobankOnlineSubServices.values()) {
            if (ecobankOnlineSubServices.getSubServiceCode().equals(test)) {
                return true;
            }
        }

        return false;
    }

    public static String getEnumByString(String subServiceCode){
        for(EcobankOnlineSubServices e : EcobankOnlineSubServices.values()){
            if(e.subServiceCode.equals(subServiceCode)) return e.name();
        }
        return null;
    }
}
