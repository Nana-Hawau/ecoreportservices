package com.ecobank.ecoreportservices.constants;

public enum ResponseEnum {


    SUCCESS("000", "Successful"),
    INVALID_HEADER_VALUES("200", "Invalid Header values provided"),
    RECORD_NOT_FOUND("201", "Record not found."),
    NOT_EXIST("900", "Service Code or Sub Service Code does not exist"),
    INVALID_REQUEST("999", "Invalid Request"),
    INVALID_DATE("999", "Date Range exceeds 90 Days");


    private String message;
    private String code;

    private ResponseEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public String getCode() {
        return code;
    }
}
