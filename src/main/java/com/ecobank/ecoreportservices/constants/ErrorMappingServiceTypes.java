package com.ecobank.ecoreportservices.constants;

public enum ErrorMappingServiceTypes {
    processInternationalTransfer("International Transfer", "INTLTRF"),
    InternationalSwiftTransfer("International Swift Transfer", "INTLSTRAF"),
    processDomesticTransfer("Domestic Transfer", "DOMTRF"),
    BILL("Bills Payment Across Group", "BILLSGROUP"),
    processSelfTransfer("Self Transfer", "STRF"),
    NIP_PAY("NIBSS Payment", "NIBPAY"),
    RTSendMoney("Rapid Transfer", "RT"),
    processInternalTransfer("Internal Transfer", "INTTRF"),
    BILL_PAY("Bills Payments", "BILLSPAY"),
    INTER_BANK ("Inter Bank", "IBANK"),
    INTER_AFF("Inter Affiliate Transfer", "IATRF"),
    DOMESTIC_TRANSFER("Domestic Transfer", "DOMTRF"),
    BILLS_PAYMENT("Bills Payment Mobile", "BPAYM"),
    INTL_TRANSFER("International Transfer", "ITRF"),
    AWA ("Cards", "Cards"),
    ENG ("Cards", "Cards"),
    FWA ("Cards", "Cards"),
    CESA1 ("Cards", "Cards"),
    CESA2 ("Cards", "Cards");

    private String subServiceName;
    private String subServiceCode;

    ErrorMappingServiceTypes(String subServiceName, String subServiceCode) {
        this.subServiceName = subServiceName;
        this.subServiceCode = subServiceCode;
    }

    public String getSubServiceName() {
        return subServiceName;
    }

    public String getSubServiceCode() {
        return subServiceCode;
    }


    public static boolean contains(String test) {
        for (ErrorMappingServiceTypes errorMappingServiceTypes : ErrorMappingServiceTypes.values()) {
            if (errorMappingServiceTypes.getSubServiceCode().equals(test)) {
                return true;
            }
        }
        return false;
    }

    public static String getEnumByString(String subServiceCode){
        for(ErrorMappingServiceTypes e : ErrorMappingServiceTypes.values()){
            if(e.subServiceCode.equals(subServiceCode)) return e.name();
        }
        return null;
    }

    public static String getString(String subServiceCode){
        for(ErrorMappingServiceTypes e : ErrorMappingServiceTypes.values()){
            if(e.subServiceCode.equals(subServiceCode)) return e.getSubServiceName();
        }
        return null;
    }
}
