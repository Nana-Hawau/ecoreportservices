package com.ecobank.ecoreportservices.constants;

public enum MobileAppSubServices {
    INTER_BANK ("Inter Bank", "IBANK"),
    INTER_AFF("Inter Affiliate Transfer", "IATRF"),
    DOMESTIC_TRANSFER("Domestic Transfer", "DOMTRF"),
    BILLS_PAYMENT("Bills Payment Mobile", "BPAYM"),
    INTL_TRANSFER("International Transfer", "ITRF");

    private String subServiceName;
    private String subServiceCode;

    MobileAppSubServices(String subServiceName, String subServiceCode) {
        this.subServiceName = subServiceName;
        this.subServiceCode = subServiceCode;
    }

    public String getSubServiceName() {
        return subServiceName;
    }

    public void setSubServiceName(String subServiceName) {
        this.subServiceName = subServiceName;
    }

    public String getSubServiceCode() {
        return subServiceCode;
    }

    public void setSubServiceCode(String subServiceCode) {
        this.subServiceCode = subServiceCode;
    }

    public static boolean contains(String test) {

        for (MobileAppSubServices mobileAppSubServices : MobileAppSubServices.values()) {
            if (mobileAppSubServices.getSubServiceCode().equals(test)) {
                return true;
            }
        }

        return false;
    }

    public static String getEnumByString(String subServiceCode){
        for(MobileAppSubServices m : MobileAppSubServices.values()){
            if(m.subServiceCode.equals(subServiceCode)) return m.name();
        }
        return null;
    }
}
