package com.ecobank.ecoreportservices.constants;

public enum Services {
    CARDS ("Cards", "Cards"),
    ECOBANK_ONLINE ("Ecobank Online", "EONLINE"),
    MOBILE_APP("Mobile Application", "MAPP");

    private final String serviceName;
    private final String serviceCode;

    Services (String serviceName, String serviceCode) {
        this.serviceName = serviceName;
        this.serviceCode = serviceCode;
    }


    public String getServiceName() {
        return serviceName;
    }


    public String getServiceCode() {
        return serviceCode;
    }

    public static boolean contains(String test) {
        for (Services services : Services.values()) {
            if (services.getServiceCode().equals(test)) {
                return true;
            }
        }
        return false;
    }

    public static String getEnumByString(String serviceCode){
        for(Services s : Services.values()){
            if(s.serviceCode.equals(serviceCode)) return s.name();
        }
        return null;
    }
}
