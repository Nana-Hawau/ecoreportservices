package com.ecobank.ecoreportservices.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.base.Predicate;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import static springfox.documentation.builders.PathSelectors.regex;
import static com.google.common.base.Predicates.or;


@Configuration
@EnableSwagger2
public class SwaggerConfig {

    /**
     *
     * @return
     */
    @Bean
    public Docket postsApi() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("ecoreportservices")
                .apiInfo(apiInfo()).select().paths((com.google.common.base.Predicate<String>) postPaths()).build();
    }

    /**
     *
     * @return
     * Predicates return a true or false value for a given input
     */
    private Predicate<String> postPaths() {
        return or(regex("/ecoreportservices/api.*"), regex("/api.*"));
    }

    /**
     *
     * @return ApiInfo Class
     * It's a class that allows you describe your APIs using additional information
     */
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title("Ecoreports Services")
                .description("Ecoreport Services")
                .termsOfServiceUrl("").build();
    }
}
