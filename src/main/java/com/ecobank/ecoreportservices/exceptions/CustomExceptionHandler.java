package com.ecobank.ecoreportservices.exceptions;

import com.auth0.jwt.exceptions.TokenExpiredException;
import com.ecobank.ecoreportservices.models.ErrorResponse;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    private static final String BAD_REQUEST = "BAD_REQUEST";

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ErrorResponse> handleConstraintViolation(
            ConstraintViolationException ex,
            WebRequest request)
    {
        List<String> details = ex.getConstraintViolations()
                .parallelStream()
                .map(ConstraintViolation::getMessage)
                .collect(Collectors.toList());
        ErrorResponse error = new ErrorResponse(BAD_REQUEST, details);
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
        logger.info(ex.getClass().getName());
        final List<String> errors = new ArrayList<String>();
        for (final FieldError error : ex.getBindingResult().getFieldErrors()) {
            errors.add(error.getField() + ": " + error.getDefaultMessage());
        }
        for (final ObjectError error : ex.getBindingResult().getGlobalErrors()) {
            errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
        }
        final ErrorResponse apiError = new ErrorResponse("Validation Failed", errors, HttpStatus.BAD_REQUEST);
        return handleExceptionInternal(ex, apiError, headers, apiError.getStatus(), request);
    }

    public ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        final ErrorResponse apiError = new ErrorResponse("Validation Failed", Arrays.asList(ex.getMessage()), HttpStatus.BAD_REQUEST);
        return handleExceptionInternal(ex, apiError, headers, apiError.getStatus(), request);
    }


    public ResponseEntity<Object> handleException (Exception ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        final ErrorResponse apiError = new ErrorResponse("An error occurred", Arrays.asList(ex.getMessage()), HttpStatus.BAD_REQUEST);
        return handleExceptionInternal(ex, apiError, headers, apiError.getStatus(), request);
    }



    public ResponseEntity<Object> handleTokenExpiredException (TokenExpiredException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        final ErrorResponse apiError = new ErrorResponse("The Token has expired", Arrays.asList(ex.getMessage()), HttpStatus.UNAUTHORIZED);
        return handleExceptionInternal(ex, apiError, headers, apiError.getStatus(), request);
    }


}
