//package com.ecobank.ecoreportservices.security.jwt;
//
//import com.ecobank.ecoreportservices.entities.Client;
//import com.ecobank.ecoreportservices.services.AuthService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.authentication.AuthenticationProvider;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.AuthenticationException;
//
//
//public class CustomAuthenticationProvider implements AuthenticationProvider {
//
//    @Autowired
//    AuthService authService;
//
//    @Override
//    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
//        String clientId = authentication.getPrincipal().toString();
//        String clientSecret = authentication.getPrincipal().toString();
//        System.out.println(clientId + clientSecret + "credentials");
//
//        Client user = authService.authenticate();
//
//        if (user == null) {
//            throw new AuthenticationException("could not login");// (3)
//        }
//        return new UsernamePasswordAuthenticationToken(clientId, clientSecret);
//    }
//
//
//    @Override
//    public boolean supports(Class<?> aClass) {
//        return aClass.equals(UsernamePasswordAuthenticationToken.class);
//    }
//}
