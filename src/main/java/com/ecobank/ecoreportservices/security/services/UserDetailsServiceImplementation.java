package com.ecobank.ecoreportservices.security.services;

import com.ecobank.ecoreportservices.entities.Client;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service(value = "UserDetailsServiceImplementation")
public class UserDetailsServiceImplementation implements UserDetailsService {


    @Override
    public UserDetails loadUserByUsername(String clientId) throws UsernameNotFoundException {

        Client client = new Client();
//        Client client = clientRepository.findByClientId(clientId)
//                .orElseThrow(() -> new UsernameNotFoundException("Client Not Found with clientId: " + clientId));
        return UserDetailsImpl.build(client);
    }
}
