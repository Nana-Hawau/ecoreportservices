package com.ecobank.ecoreportservices.security.services;

import com.ecobank.ecoreportservices.entities.Client;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;



public class UserDetailsImpl implements UserDetails {
    private static final long serialVersionUID = 1L;

    private Long id;

    private String clientId;

    private String sourceCode;

    @JsonIgnore
    private String clientSecret;

    private Collection<? extends GrantedAuthority> authorities;

    public UserDetailsImpl(Long id, String clientId, String sourceCode, String clientSecret) {
        this.id = id;
        this.clientId = clientId;
        this.sourceCode = sourceCode;
        this.clientSecret = clientSecret;
    }

    public static UserDetailsImpl build(Client client) {
        List<GrantedAuthority> authorities = new ArrayList<>();


        return new UserDetailsImpl(
                client.getId(),
                client.getClientId(),
                client.getClientSecret(),
                client.getSourceCode()
                );
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public String getUsername() {
        return null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
        this.authorities = authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        UserDetailsImpl user = (UserDetailsImpl) o;
        return Objects.equals(id, user.id);
    }

    @Override
    public String toString() {
        return "UserDetailsImpl{" +
                "id=" + id +
                ", clientId='" + clientId + '\'' +
                ", sourceCode='" + sourceCode + '\'' +
                ", clientSecret='" + clientSecret + '\'' +
                ", authorities=" + authorities +
                '}';
    }
}
