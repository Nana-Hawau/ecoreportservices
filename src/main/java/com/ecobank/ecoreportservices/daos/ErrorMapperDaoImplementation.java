package com.ecobank.ecoreportservices.daos;

import com.ecobank.ecoreportservices.models.MappedErrorRespModel;
import com.ecobank.ecoreportservices.utils.AffiliateDataSources;
import com.ecobank.ecoreportservices.utils.CloseDbConnection;
import org.slf4j.Logger;
import oracle.jdbc.internal.OracleTypes;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;

@Repository
public class ErrorMapperDaoImplementation {
    final static Logger LOGGER = LoggerFactory.getLogger(ErrorMapperDaoImplementation.class);

    @Autowired
    private AffiliateDataSources dataSource;

    @Autowired
    private CloseDbConnection closeConnection;


    @Value("${sadcwarrikey}")
    private String oracleJndiKey;


    public MappedErrorRespModel getMappedErrorFields (String serviceName, String errorCode) {
        Connection connection = null;
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;

        MappedErrorRespModel response = null;

        try {
            connection = dataSource.getDataSource(oracleJndiKey).getConnection();
            String query = "{call  eco_rpt_two_pkg.get_mapped_error_fields(?,?,?,?,?)}";
            callableStatement = connection.prepareCall(query);
            callableStatement.setString(1, serviceName);
            callableStatement.setString(2, errorCode);
            callableStatement.registerOutParameter(3, Types.VARCHAR);
            callableStatement.registerOutParameter(4, Types.VARCHAR);
            callableStatement.registerOutParameter(5, OracleTypes.CURSOR);

            callableStatement.execute();

            resultSet = (ResultSet) callableStatement.getObject(5);

            while (resultSet.next()) {

                response = new MappedErrorRespModel();

                response.setErrorCode(resultSet.getString("error_code"));
                response.setErrorDesc(resultSet.getString("error_description"));
                response.setProposedResolution(resultSet.getString("proposed_resolution"));
                response.setServiceName(resultSet.getString("service_name"));
                response.setErrorCategory(resultSet.getString("error_category"));
            }

            System.out.println("DAO Response >>>>>>>>>>>>>>>>>>>>>>> " + response);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection.closeDbConnection(connection, callableStatement, resultSet);
        }

        return response;
    }

}
