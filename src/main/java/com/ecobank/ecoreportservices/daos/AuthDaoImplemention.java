package com.ecobank.ecoreportservices.daos;

import com.ecobank.ecoreportservices.constants.ResponseEnum;
import com.ecobank.ecoreportservices.exceptions.BadHeaderValuesException;
import com.ecobank.ecoreportservices.models.AesKeyPair;
import com.ecobank.ecoreportservices.utils.AffiliateDataSources;
import com.ecobank.ecoreportservices.utils.CloseDbConnection;
import lombok.extern.slf4j.Slf4j;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

@Slf4j
@Service
public class AuthDaoImplemention {


    @Autowired
    private AffiliateDataSources dataSource;

    @Value("${ENG}")
    private String oracleJndiKey;

    @Autowired
    private CloseDbConnection closeConnection;

    public AesKeyPair authenticateClient (String clientId, String clientSecret, String sourceCode) throws SQLException, BadHeaderValuesException {
        Connection conn = null;
        OracleCallableStatement cStmt = null;
        ResultSet resultSet = null;
        AesKeyPair keyPair = new AesKeyPair();

        try {

            conn = dataSource.getDataSource(oracleJndiKey).getConnection();

            cStmt = (OracleCallableStatement) conn.prepareCall("{? = call eco_dmm_pkg.authenticateClient(?,?,?,?)}");
            cStmt.registerOutParameter(1, OracleTypes.CURSOR);
            cStmt.setString(2, clientId);
            cStmt.setString(3, clientSecret);
            cStmt.setString(4, sourceCode);
            cStmt.registerOutParameter(5, OracleTypes.VARCHAR);

            cStmt.execute();
            String responseCode = cStmt.getString(5);

            if(responseCode.equals("99")) {
                throw new BadHeaderValuesException(ResponseEnum.INVALID_HEADER_VALUES);
            }else{
                resultSet = (ResultSet) cStmt.getObject(1);
                while (resultSet.next()) {
                    keyPair = new AesKeyPair();
                    keyPair.setPublicKey(resultSet.getString("PARTNER_PUBLIC_KEY").replaceAll("\\n", ""));
                    keyPair.setPrivateKey(resultSet.getString("PRIVATE_KEY").replaceAll("\\n", ""));
                    keyPair.setSecretKey(resultSet.getString("SECRET_KEY").replaceAll("\\n", ""));
                }
            }

        } catch (Exception e) {
            log.error(e.toString());
            throw e;
        } finally {
            closeConnection.closeDbConnection(conn, cStmt, resultSet);
        }

        return keyPair;
    }
}
