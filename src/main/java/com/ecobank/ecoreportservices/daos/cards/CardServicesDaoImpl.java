package com.ecobank.ecoreportservices.daos.cards;


import com.ecobank.ecoreportservices.models.CardsSearchDataRequest;
import com.ecobank.ecoreportservices.models.CardsSearchDataRespModel;
import com.ecobank.ecoreportservices.models.CardsSearchModalDataRespModel;
import com.ecobank.ecoreportservices.utils.AffiliateDataSources;
import com.ecobank.ecoreportservices.utils.CloseDbConnection;
import oracle.jdbc.internal.OracleTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;


@Repository
public class CardServicesDaoImpl {
    final static Logger LOGGER = LoggerFactory.getLogger(CardServicesDaoImpl.class);


    @Autowired
    private AffiliateDataSources dataSource;


    @Autowired
    private CloseDbConnection closeConnection;

    @Value("${db.package}")
    private String dataBasePackage;

    public List<CardsSearchDataRespModel> getSearchDataCards (CardsSearchDataRequest cardsSearchDataRequest){

        Connection connection = null;
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;

        String responseCode;
        String responseMessage;
        List<CardsSearchDataRespModel> response = new ArrayList<>();
        CardsSearchDataRespModel detail = null;

        String startDate = cardsSearchDataRequest.getStartDate();
        String endDate = cardsSearchDataRequest.getEndDate();
        Integer recordLimit = cardsSearchDataRequest.getRecordLimit();
        String cardAccount = cardsSearchDataRequest.getCardAccount();
        BigDecimal amount = cardsSearchDataRequest.getAmount();
        String transactionRefNumber = cardsSearchDataRequest.getTransactionRefNumber();

        try {
            connection = dataSource.getDataSource(cardsSearchDataRequest.getCluster()).getConnection();
            String query = "{call " + dataBasePackage + ".get_search_data_cards(?,?,?,?,?,?,?,?,?)}";
            callableStatement = connection.prepareCall(query);

            callableStatement.setString(1, startDate);
            callableStatement.setString(2, endDate);
            callableStatement.setInt(3, recordLimit);
            callableStatement.setString(4, cardAccount);
            callableStatement.setBigDecimal(5, amount);
            callableStatement.setString(6, transactionRefNumber);
            callableStatement.registerOutParameter(7, Types.VARCHAR);
            callableStatement.registerOutParameter(8, Types.VARCHAR);
            callableStatement.registerOutParameter(9, OracleTypes.CURSOR);

            callableStatement.execute();

            responseCode = callableStatement.getString(7);
            responseMessage = callableStatement.getString(8);

            LOGGER.info("responseCode >>>>>>>>>>>>>>>>>>>>>>> " + responseCode);
            LOGGER.info("responseMessage >>>>>>>>>>>>>>>>>>>>>>> " + responseMessage);

            resultSet = (ResultSet) callableStatement.getObject(9);

            if(responseCode.equals("000") && responseMessage.equals("SUCCESS")){

                while (resultSet.next()) {

                    detail = new CardsSearchDataRespModel();

                    detail.setAccountName(resultSet.getString("acct_name"));
                    detail.setCardAccount(resultSet.getString("card_account"));
                    detail.setTransactionRefNumber(resultSet.getString("tran_ref_no"));
                    detail.setAmount(resultSet.getBigDecimal("amount"));
                    detail.setCurrency(resultSet.getString("currency"));
                    detail.setTransactionDate(resultSet.getString("trans_date"));
                    detail.setTerminalId(resultSet.getString("terminal_id"));
                    detail.setReturnReferenceNumber(resultSet.getString("ret_ref_num"));
                    detail.setSolId(resultSet.getString("sol_id"));
                    detail.setAtmOwner(resultSet.getString("atm_owner"));
                    detail.setTypeOfTransaction(resultSet.getString("type_of_trans"));
                    detail.setAtmTransactionType(resultSet.getString("atm_tran_type"));
                    detail.setStan(resultSet.getString("stan"));
                    detail.setPan(resultSet.getString("pan"));
                    detail.setTransactionStatus(resultSet.getString("transaction_status"));
                    detail.setErrorCode(resultSet.getString("error_code"));
                    detail.setErrorMessage(resultSet.getString("error_message"));
                    detail.setRegion(resultSet.getString("region"));

                    response.add(detail);
                }

            }

            LOGGER.info("DAO Response >>>>>>>>>>>>>>>>>>>>>>> " + response);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection.closeDbConnection(connection, callableStatement, resultSet);
        }

        return response;
    }



}
