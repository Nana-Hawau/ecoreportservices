package com.ecobank.ecoreportservices.daos;

import com.ecobank.ecoreportservices.models.AffiliateListModel;
import com.ecobank.ecoreportservices.utils.AffiliateDataSources;
import com.ecobank.ecoreportservices.utils.CloseDbConnection;
import oracle.jdbc.internal.OracleTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

@Repository
public class AffiliateListDaoImplementation {

    final static Logger LOGGER = LoggerFactory.getLogger(AffiliateListDaoImplementation.class);

    @Autowired
    private AffiliateDataSources dataSource;

    @Autowired
    private CloseDbConnection closeConnection;


    @Value("${sadcwarrikey}")
    private String oracleJndiKey;

    public List<AffiliateListModel> getAffiliateList () {

        Connection connection = null;
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;

        String responseCode;
        String responseMessage;
        List<AffiliateListModel> response = new ArrayList<>();
        AffiliateListModel detail = null;

        try {
            connection = dataSource.getDataSource(oracleJndiKey).getConnection();
            String query = "{call  eco_rpt_two_pkg.get_affiliate_list_ea(?,?,?)}";
            callableStatement = connection.prepareCall(query);

            callableStatement.registerOutParameter(1, Types.VARCHAR);
            callableStatement.registerOutParameter(2, Types.VARCHAR);
            callableStatement.registerOutParameter(3, OracleTypes.CURSOR);
            callableStatement.execute();

            responseCode = callableStatement.getString(1);
            responseMessage = callableStatement.getString(2);

            LOGGER.debug("responseCode >>>>>>>>>>>>>>>>>>>>>>> " + responseCode);
            LOGGER.debug("responseMessage >>>>>>>>>>>>>>>>>>>>>>> " + responseMessage);

            resultSet = (ResultSet)callableStatement.getObject(3);

            if(responseCode.equals("000") && responseMessage.equals("SUCCESS")){

                while (resultSet.next()) {

                    detail = new AffiliateListModel();

                    detail.setCountryCode(resultSet.getString("country_code"));
                    detail.setCountry(resultSet.getString("country"));

                    response.add(detail);
                }

                LOGGER.debug("DAO Response >>>>>>>>>>>>>>>>>>>>>>> " + response);

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection.closeDbConnection(connection, callableStatement, resultSet);
        }

        return response;
    }

}
