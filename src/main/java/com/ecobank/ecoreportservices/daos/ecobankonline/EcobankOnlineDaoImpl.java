package com.ecobank.ecoreportservices.daos.ecobankonline;

import com.ecobank.ecoreportservices.models.EcobankOnlineTransResponse;
import com.ecobank.ecoreportservices.utils.AffiliateDataSources;
import com.ecobank.ecoreportservices.utils.CloseDbConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import oracle.jdbc.internal.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

@Repository
public class EcobankOnlineDaoImpl {


    final static Logger LOGGER = LoggerFactory.getLogger(EcobankOnlineDaoImpl.class);


    @Autowired
    private AffiliateDataSources dataSource;


    @Value("${obdxkey}")
    private String oracleJndiKey;

    @Autowired
    private CloseDbConnection closeConnection;

    public List<EcobankOnlineTransResponse> getEcobankOnlineTransactions(String referenceNumber,
                                                                         String startDate, String endDate, String sourceAccount,
                                                                         String serviceName,
                                                                         String amount, String beneficiaryacc,
                                                                         String affiliateCode, Integer recordLimit){
        Connection connection = null;
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;

        String responseCode;
        String responseMessage;
        List<EcobankOnlineTransResponse> response = new ArrayList<>();
        EcobankOnlineTransResponse detail = null;

        try {
            connection = dataSource.getDataSource(oracleJndiKey).getConnection();
            //connection= cardOracleDataSource().getConnection();
            String query = "{call  ecoreport_ecobank_online_pkg.ecobank_online(?,?,?,?,?,?,?,?,?,?,?,?)}";
            callableStatement = connection.prepareCall(query);
            callableStatement.setString(1, referenceNumber);
            callableStatement.setString(2, startDate);
            callableStatement.setString(3, endDate);
            callableStatement.setString(4, sourceAccount);
            callableStatement.setString(5, serviceName);
            callableStatement.setString(6,amount);
            callableStatement.setString(7,beneficiaryacc);
            callableStatement.setString(8,affiliateCode);
            callableStatement.setInt(9,    recordLimit);
            callableStatement.registerOutParameter(10, Types.VARCHAR);
            callableStatement.registerOutParameter(11, Types.VARCHAR);
            callableStatement.registerOutParameter(12, OracleTypes.CURSOR);

            callableStatement.execute();
            responseMessage = callableStatement.getString(10);
            responseCode = callableStatement.getString(11);


            LOGGER.info("responseCode >>>>>>>>>>>>>>>>>>>>>>> " + responseCode);
            LOGGER.info("responseMessage >>>>>>>>>>>>>>>>>>>>>>> " + responseMessage);

            resultSet = (ResultSet) callableStatement.getObject(12);
            LOGGER.info("RESULSET   SIZE=======>"+resultSet.getFetchSize());

            System.out.println("got here 1");
            if(responseCode.equals("000") && responseMessage.equals("SUCCESS")){
                System.out.println("got here 2");

                while (resultSet.next()) {


                    detail = new EcobankOnlineTransResponse();



                    detail.setServiceName(resultSet.getString("SERVICE_NAME"));
                    detail.setStartDate(resultSet.getString("START_DATE_TIME"));
                    detail.setEndDate(resultSet.getString("END_DATE_TIME"));
                    detail.setReference(resultSet.getString("REFERENCENO"));
                    detail.setCountry(resultSet.getString("AFFILIATE_CODE"));
                    detail.setAccountNumber(resultSet.getString("sourceAccount"));
                    detail.setCustomerName(resultSet.getString("CUSTOMERNAME"));
                    detail.setAmount(resultSet.getString("AMOUNT"));
                    //detail.setPaymentDate(resultSet.getString("paymentDate"));
                    detail.setResponseCode(resultSet.getString("RESPONSECODE"));
                    detail.setResponseMsg(resultSet.getString("RESPONSEMESSAGE"));

                    response.add(detail);


               }

            }
            System.out.println("DAO Response >>>>>>>>>>>>>>>>>>>>>>> " +response);


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection.closeDbConnection(connection, callableStatement, resultSet);
        }

        return response;
    }

    public List<EcobankOnlineTransResponse> getRTNIPTransactions(String referenceNumber,
                                                                         String startDate, String endDate, String sourceAccount,
                                                                         String serviceName,
                                                                         String amount, String beneficiaryacc,
                                                                         String affiliateCode,Integer recordLimit){
        Connection connection = null;
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;

        String responseCode;
        String responseMessage;
        List<EcobankOnlineTransResponse> response = new ArrayList<>();
        EcobankOnlineTransResponse detail = null;

        try {
            connection = dataSource.getDataSource(oracleJndiKey).getConnection();
            //connection= cardOracleDataSource().getConnection();
            String query = "{call  ecoreport_ecobank_online_pkg.ecobank_online(?,?,?,?,?,?,?,?,?,?,?,?)}";
            callableStatement = connection.prepareCall(query);



            callableStatement.setString(1, referenceNumber);
            callableStatement.setString(2, startDate);
            callableStatement.setString(3, endDate);
            callableStatement.setString(4, sourceAccount);
            callableStatement.setString(5, serviceName);
            callableStatement.setString(6,amount);
            callableStatement.setString(7,beneficiaryacc);
            callableStatement.setString(8,affiliateCode);
            callableStatement.setInt(9,    recordLimit);
            callableStatement.registerOutParameter(10, Types.VARCHAR);
            callableStatement.registerOutParameter(11, Types.VARCHAR);
            callableStatement.registerOutParameter(12, OracleTypes.CURSOR);

            callableStatement.execute();
            responseMessage = callableStatement.getString(10);
            responseCode = callableStatement.getString(11);


            LOGGER.info("responseCode >>>>>>>>>>>>>>>>>>>>>>> " + responseCode);
            LOGGER.info("responseMessage >>>>>>>>>>>>>>>>>>>>>>> " + responseMessage);

            resultSet = (ResultSet) callableStatement.getObject(12);
            LOGGER.info("RESULSET   SIZE=======>"+resultSet.getFetchSize());

            System.out.println("got here 1");
            if(responseCode.equals("000") && responseMessage.equals("SUCCESS")){
                System.out.println("got here 2");

                while (resultSet.next()) {


                    detail = new EcobankOnlineTransResponse();



                    detail.setServiceName(resultSet.getString("SERVICE_NAME"));
                    detail.setStartDate(resultSet.getString("START_DATE_TIME"));
                    detail.setEndDate(resultSet.getString("END_DATE_TIME"));
                    detail.setReference(resultSet.getString("REFERENCENO"));
                    detail.setCountry(resultSet.getString("AFFILIATE_CODE"));
                    detail.setAccountNumber(resultSet.getString("sourceAccount"));
                    detail.setCustomerName(resultSet.getString("CUSTOMERNAME"));
                    detail.setAmount(resultSet.getString("AMOUNT"));
                    detail.setBeneficiaryAccountNo(resultSet.getString("beneficiaryAccountNo"));
                    detail.setBeneficiaryname(resultSet.getString("beneficiaryName"));
                    detail.setResponseCode(resultSet.getString("RESPONSECODE"));
                    detail.setResponseMsg(resultSet.getString("RESPONSEMESSAGE"));

                    response.add(detail);


                }

            }
            System.out.println("DAO Response >>>>>>>>>>>>>>>>>>>>>>> " +response);

            //LOGGER.info("DAO Response >>>>>>>>>>>>>>>>>>>>>>> " + response);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection.closeDbConnection(connection, callableStatement, resultSet);
        }

        return response;
    }







    public EcobankOnlineTransResponse getBillTransactionModal ( String referenceNumber,
                                                          String serviceName){
        Connection connection = null;
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;

        String responseCode;
        String responseMessage;
        EcobankOnlineTransResponse detail = null;
        try {
            connection = dataSource.getDataSource(oracleJndiKey).getConnection();
            //connection= cardOracleDataSource().getConnection();
            String query = "{call ecoreport_ecobank_online_pkg.get_transaction_modal(?,?,?,?,?)}";
            callableStatement = connection.prepareCall(query);

            callableStatement.setString(1, referenceNumber);
            callableStatement.setString(2,serviceName);
            callableStatement.registerOutParameter(3, Types.VARCHAR);
            callableStatement.registerOutParameter(4, Types.VARCHAR);
            callableStatement.registerOutParameter(5, OracleTypes.CURSOR);

            callableStatement.execute();

            responseCode = callableStatement.getString(3);
            responseMessage = callableStatement.getString(4);

            LOGGER.info("responseCode >>>>>>>>>>>>>>>>>>>>>>> " + responseCode);
            LOGGER.info("responseMessage >>>>>>>>>>>>>>>>>>>>>>> " + responseMessage);

            resultSet = (ResultSet) callableStatement.getObject(5);

            while (resultSet.next()) {
                detail = new EcobankOnlineTransResponse();





                detail.setReference(resultSet.getString("CUSTOMERNAME"));
                detail.setAccountNumber(resultSet.getString("SOURCEACCOUNT"));
                detail.setStartDate(resultSet.getString("PAYMENTDATE"));
                detail.setReference(resultSet.getString("REFERENCENO"));
                detail.setCountry(resultSet.getString("AFFILIATE_CODE"));
                detail.setServiceName(resultSet.getString("SERVICE_NAME"));
              //  detail.setUserId(resultSet.getString("USERID"));
                detail.setBillerCode(resultSet.getString("BILLER_CODE"));
                detail.setRequestId(resultSet.getString("REQUEST_ID"));
                detail.setRequestToken(resultSet.getString("REQUEST_TOKEN"));
                detail.setRequestType(resultSet.getString("REQUEST_TYPE"));
                detail.setChannel(resultSet.getString("CHANNEL"));
                detail.setBillerCode(resultSet.getString("BILLER_CODE"));
                detail.setBillerId(resultSet.getString("BILLER_ID"));
                detail.setBillerRefNo(resultSet.getString("BILLREFNO"));
                detail.setCbaPostingRequired(resultSet.getString("CBAPOSTINGREQUIRED"));
                detail.setCustomerRefNo(resultSet.getString("CUSTOMERREFNO"));
                detail.setProductCode(resultSet.getString("PRODUCTCODE"));
                detail.setAmount(resultSet.getString("AMOUNT"));
                detail.setCurrency(resultSet.getString("CCY"));
                detail.setPaymentDesciption(resultSet.getString("PAYMENTDESCRIPTION"));
                detail.setSourceCode(resultSet.getString("SOURCECODE"));
                detail.setResponseCode(resultSet.getString("RESPONSECODE"));
                detail.setResponseMsg(resultSet.getString("RESPONSEMESSAGE"));

            }


            LOGGER.info("DAO Response >>>>>>>>>>>>>>>>>>>>>>> " + detail);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection.closeDbConnection(connection, callableStatement, resultSet);
        }


        return detail;
    }

    public EcobankOnlineTransResponse getRTTransactionModal ( String referenceNumber,
                                                                String serviceName){
        Connection connection = null;
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;

        String responseCode;
        String responseMessage;
        EcobankOnlineTransResponse detail = null;
        try {
            connection = dataSource.getDataSource(oracleJndiKey).getConnection();
            //connection= cardOracleDataSource().getConnection();
            String query = "{call ecoreport_ecobank_online_pkg.get_transaction_modal(?,?,?,?,?)}";
            callableStatement = connection.prepareCall(query);

            callableStatement.setString(1, referenceNumber);
            callableStatement.setString(2,serviceName);
            callableStatement.registerOutParameter(3, Types.VARCHAR);
            callableStatement.registerOutParameter(4, Types.VARCHAR);
            callableStatement.registerOutParameter(5, OracleTypes.CURSOR);

            callableStatement.execute();

            responseCode = callableStatement.getString(3);
            responseMessage = callableStatement.getString(4);

            LOGGER.info("responseCode >>>>>>>>>>>>>>>>>>>>>>> " + responseCode);
            LOGGER.info("responseMessage >>>>>>>>>>>>>>>>>>>>>>> " + responseMessage);

            resultSet = (ResultSet) callableStatement.getObject(5);

            while (resultSet.next()) {
                detail = new EcobankOnlineTransResponse();

                detail.setServiceName(resultSet.getString("SERVICE_NAME"));

                detail.setStartDate(resultSet.getString("START_DATE_TIME"));
                detail.setEndDate(resultSet.getString("END_DATE_TIME"));
                detail.setReference(resultSet.getString("REFERENCENO"));
                detail.setCountry(resultSet.getString("AFFILIATE_CODE"));
                detail.setAccountNumber(resultSet.getString("SOURCEACCOUNT"));
                detail.setCustomerName(resultSet.getString("CUSTOMERNAME"));
                detail.setAmount(resultSet.getString("AMOUNT"));
                detail.setPaymentDate(resultSet.getString("PAYMENTDATE"));
                detail.setResponseCode(resultSet.getString("RESPONSECODE"));

                detail.setResponseMsg(resultSet.getString("RESPONSEMESSAGE"));
                detail.setDestinationCountry(resultSet.getString("destCountry"));
                detail.setBeneficiaryAccountNo(resultSet.getString("beneficiaryAccountNo"));
                detail.setBeneficiaryname(resultSet.getString("beneficiaryname"));

//
            }


            LOGGER.info("DAO Response >>>>>>>>>>>>>>>>>>>>>>> " + detail);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection.closeDbConnection(connection, callableStatement, resultSet);
        }


        return detail;
    }

    public EcobankOnlineTransResponse getNIPPAYTransactionModal ( String referenceNumber,
                                                                    String serviceName){
        Connection connection = null;
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;

        String responseCode;
        String responseMessage;
        EcobankOnlineTransResponse detail = null;
        try {
            connection = dataSource.getDataSource(oracleJndiKey).getConnection();
            //connection= cardOracleDataSource().getConnection();
            String query = "{call ecoreport_ecobank_online_pkg.get_transaction_modal(?,?,?,?,?)}";
            callableStatement = connection.prepareCall(query);

            callableStatement.setString(1, referenceNumber);
            callableStatement.setString(2,serviceName);
            callableStatement.registerOutParameter(3, Types.VARCHAR);
            callableStatement.registerOutParameter(4, Types.VARCHAR);
            callableStatement.registerOutParameter(5, OracleTypes.CURSOR);

            callableStatement.execute();

            responseCode = callableStatement.getString(3);
            responseMessage = callableStatement.getString(4);

            LOGGER.info("responseCode >>>>>>>>>>>>>>>>>>>>>>> " + responseCode);
            LOGGER.info("responseMessage >>>>>>>>>>>>>>>>>>>>>>> " + responseMessage);

            resultSet = (ResultSet) callableStatement.getObject(5);

            while (resultSet.next()) {
                detail = new EcobankOnlineTransResponse();


                detail.setServiceName(resultSet.getString("SERVICE_NAME"));
                detail.setStartDate(resultSet.getString("START_DATE_TIME"));
                detail.setEndDate(resultSet.getString("END_DATE_TIME"));
                detail.setReference(resultSet.getString("REFERENCENO"));
                detail.setCountry(resultSet.getString("AFFILIATE_CODE"));
                detail.setAccountNumber(resultSet.getString("SOURCEACCOUNT"));
                detail.setCustomerName(resultSet.getString("CUSTOMERNAME"));
                detail.setAmount(resultSet.getString("AMOUNT"));
               // detail.setPaymentDate(resultSet.getString("PAYMENTDATE"));
                detail.setResponseCode(resultSet.getString("RESPONSECODE"));
                detail.setCurrency(resultSet.getString("CCY"));
                detail.setResponseMsg(resultSet.getString("RESPONSEMESSAGE"));
                detail.setSourceCode(resultSet.getString("SOURCECODE"));
                detail.setRequestId(resultSet.getString("REQUESTID"));
               detail.setRequestToken(resultSet.getString("REQUESTTOKEN"));
               detail.setRequestType(resultSet.getString("REQUESTTYPE"));
               detail.setNarration(resultSet.getString("narration"));
               detail.setSendingBankCode(resultSet.getString("sendingBankCode"));
               detail.setBeneficiaryname(resultSet.getString("beneficiaryName"));
               detail.setDestinationBankCode(resultSet.getString("destinationBankCode"));
               detail.setBeneficiaryAccountNo(resultSet.getString("beneficiaryAccountNo"));
               detail.setTransferNo(resultSet.getString("transferNo"));
                detail.setTransactionId(resultSet.getString("transactionId"));
               detail.setTransactionReferenceNumber(resultSet.getString("transactionReferenceNumber"));
                detail.setPaymentReferenceNo(resultSet.getString("paymentReferenceNo"));

            }


            LOGGER.info("DAO Response >>>>>>>>>>>>>>>>>>>>>>> " + detail);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection.closeDbConnection(connection, callableStatement, resultSet);
        }


        return detail;
    }


    public EcobankOnlineTransResponse getTransferTransactionModal ( String referenceNumber,
                                                                String serviceName){
        Connection connection = null;
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;

        String responseCode;
        String responseMessage;
        EcobankOnlineTransResponse detail = null;
        try {
            connection = dataSource.getDataSource(oracleJndiKey).getConnection();
           // connection= cardOracleDataSource().getConnection();
            String query = "{call ecoreport_ecobank_online_pkg.get_transaction_modal(?,?,?,?,?)}";
            callableStatement = connection.prepareCall(query);

            callableStatement.setString(1, referenceNumber);
            callableStatement.setString(2,serviceName);
            callableStatement.registerOutParameter(3, Types.VARCHAR);
            callableStatement.registerOutParameter(4, Types.VARCHAR);
            callableStatement.registerOutParameter(5, OracleTypes.CURSOR);

            callableStatement.execute();

            responseCode = callableStatement.getString(3);
            responseMessage = callableStatement.getString(4);

            LOGGER.info("responseCode >>>>>>>>>>>>>>>>>>>>>>> " + responseCode);
            LOGGER.info("responseMessage >>>>>>>>>>>>>>>>>>>>>>> " + responseMessage);

            resultSet = (ResultSet) callableStatement.getObject(5);

            while (resultSet.next()) {
                detail = new EcobankOnlineTransResponse();


                detail.setServiceName(resultSet.getString("SERVICE_NAME"));

                detail.setStartDate(resultSet.getString("START_DATE_TIME"));
                detail.setEndDate(resultSet.getString("END_DATE_TIME"));
                detail.setReference(resultSet.getString("REFERENCENO"));
                detail.setCountry(resultSet.getString("AFFILIATE_CODE"));
                detail.setAccountNumber(resultSet.getString("cusacno"));
                detail.setCustomerName(resultSet.getString("custnam"));
                detail.setAmount(resultSet.getString("AMOUNT"));
                detail.setPaymentDate(resultSet.getString("PAYMENTDATE"));
                detail.setResponseCode(resultSet.getString("message"));
                detail.setResponseMsg(resultSet.getString("edesc"));
                detail.setTxnccy(resultSet.getString("txnccy"));
                detail.setMsgid(resultSet.getString("msgid"));
                detail.setBranch(resultSet.getString("branch"));
                detail.setRemrk(resultSet.getString("remrk"));
                detail.setCpacno(resultSet.getString("cpacno"));
                detail.setCounterprtyname(resultSet.getString("counterprtyname"));
                detail.setActamt(resultSet.getString("actamt"));
                detail.setAuthorizationstatus(resultSet.getString("authorizationstatus"));
                detail.setEntity(resultSet.getString("entity"));
                detail.setTxnamt(resultSet.getString("txnamt"));
                detail.setBankname(resultSet.getString("bankname"));
                detail.setAuthorizationstatus(resultSet.getString("authorizationstatus"));
                detail.setActdt(resultSet.getString("actdt"));
                detail.setCatgpurptyp(resultSet.getString("catgpurptyp"));
                detail.setCusacccy(resultSet.getString("cusacccy"));
                detail.setCusaclcf(resultSet.getString("cusaclcf"));
                detail.setCusavlbal(resultSet.getString("cusavlbal"));
                detail.setAcdesc((resultSet.getString("acdesc")));



            }


            LOGGER.info("DAO Response >>>>>>>>>>>>>>>>>>>>>>> " + detail);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection.closeDbConnection(connection, callableStatement, resultSet);
        }


        return detail;
    }



}
