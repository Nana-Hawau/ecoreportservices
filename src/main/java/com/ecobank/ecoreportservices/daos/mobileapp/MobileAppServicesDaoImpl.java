package com.ecobank.ecoreportservices.daos.mobileapp;

import com.ecobank.ecoreportservices.models.MobileAppModalRespModel;
import com.ecobank.ecoreportservices.models.MobileAppRespModel;
import com.ecobank.ecoreportservices.utils.AffiliateDataSources;
import com.ecobank.ecoreportservices.utils.CloseDbConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import oracle.jdbc.internal.OracleTypes;

@Repository
public class MobileAppServicesDaoImpl {
    final static Logger LOGGER = LoggerFactory.getLogger(MobileAppServicesDaoImpl.class);


    @Autowired
    private AffiliateDataSources dataSource;

    @Value("${sadcwarrikey}")
    private String oracleJndiKey;


    @Autowired
    private CloseDbConnection closeConnection;

    public List<MobileAppRespModel> getSearchTransDataInterAff (String startDate, String endDate, BigDecimal recordLimit, String transactionRef,
                                                                BigDecimal amount, String customerAccNo, String beneficiaryAccNo, String affiliate){
        Connection connection = null;
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;

        String responseCode;
        String responseMessage;
        List<MobileAppRespModel> response = new ArrayList<>();
        MobileAppRespModel detail = null;

        try {
            connection = dataSource.getDataSource(oracleJndiKey).getConnection();
            //connection = cardOracleDataSource().getConnection();
            String query = "{call eco_rpt_two_pkg.get_search_mobile_app_iat(?,?,?,?,?,?,?,?,?,?,?)}";
            callableStatement = connection.prepareCall(query);
            callableStatement.setString(1, startDate);
            callableStatement.setString(2, endDate);
            callableStatement.setBigDecimal(3, recordLimit);
            callableStatement.setString(4, transactionRef);
            callableStatement.setBigDecimal(5, amount);
            callableStatement.setString(6, customerAccNo);
            callableStatement.setString(7, beneficiaryAccNo);
            callableStatement.setString(8, affiliate);
            callableStatement.registerOutParameter(9, Types.VARCHAR);
            callableStatement.registerOutParameter(10, Types.VARCHAR);
            callableStatement.registerOutParameter(11, OracleTypes.CURSOR);
            callableStatement.execute();

            responseCode = callableStatement.getString(9);
            responseMessage = callableStatement.getString(10);

            LOGGER.debug("responseCode >>>>>>>>>>>>>>>>>>>>>>> " + responseCode);
            LOGGER.debug("responseMessage >>>>>>>>>>>>>>>>>>>>>>> " + responseMessage);

            resultSet = (ResultSet) callableStatement.getObject(11);

            if(responseCode.equals("000") && responseMessage.equals("SUCCESS")){

                while (resultSet.next()) {

                    detail = new MobileAppRespModel();

                    detail.setTransactionRef(resultSet.getString("transaction_ref"));
                    detail.setTranDateTime(resultSet.getString("trans_date_time"));
                    detail.setCustomerAccNo(resultSet.getString("customer_acc_no"));
                    detail.setRespomseCode(resultSet.getString("error_code"));
                    detail.setResponseDesc(resultSet.getString("error_message"));
                    detail.setCountryCode(resultSet.getString("country_code"));
                    detail.setAmount(resultSet.getString("amount"));
                    detail.setBeneficiaryAccNo(resultSet.getString("BENEFICIARY_ACCOUNT_NUMBER"));
                    detail.setCcy(resultSet.getString("TRANSFER_CURRENCY"));

                    response.add(detail);
                }

            }

            System.out.println("DAO Response >>>>>>>>>>>>>>>>>>>>>>> " + response);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection.closeDbConnection(connection, callableStatement, resultSet);
        }

        return response;
    }

    public List<MobileAppRespModel> getSearchTransDataInternationalTrans (String startDate, String endDate, BigDecimal recordLimit, String requestId,
                                                                BigDecimal amount, String customerAccNo, String beneficiaryAccNo, String affiliate){
        Connection connection = null;
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;

        String responseCode;
        String responseMessage;
        List<MobileAppRespModel> response = new ArrayList<>();
        MobileAppRespModel detail = null;

        try {
            connection = dataSource.getDataSource(oracleJndiKey).getConnection();
            //connection = cardOracleDataSource().getConnection();
            String query = "{call eco_rpt_two_pkg.get_search_mobile_app_itt(?,?,?,?,?,?,?,?,?,?,?)}";
            callableStatement = connection.prepareCall(query);
            callableStatement.setString(1, startDate);
            callableStatement.setString(2, endDate);
            callableStatement.setBigDecimal(3, recordLimit);
            callableStatement.setString(4, requestId);
            callableStatement.setBigDecimal(5, amount);
            callableStatement.setString(6, customerAccNo);
            callableStatement.setString(7, beneficiaryAccNo);
            callableStatement.setString(8, affiliate);
            callableStatement.registerOutParameter(9, Types.VARCHAR);
            callableStatement.registerOutParameter(10, Types.VARCHAR);
            callableStatement.registerOutParameter(11, OracleTypes.CURSOR);
            callableStatement.execute();

            responseCode = callableStatement.getString(9);
            responseMessage = callableStatement.getString(10);

            LOGGER.debug("responseCode >>>>>>>>>>>>>>>>>>>>>>> " + responseCode);
            LOGGER.debug("responseMessage >>>>>>>>>>>>>>>>>>>>>>> " + responseMessage);

            resultSet = (ResultSet) callableStatement.getObject(11);

            if(responseCode.equals("000") && responseMessage.equals("SUCCESS")){

                while (resultSet.next()) {

                    detail = new MobileAppRespModel();

                    detail.setRequestId(resultSet.getString("request_id"));
                    detail.setTranDateTime(resultSet.getString("trans_date_time"));
                    detail.setCustomerAccNo(resultSet.getString("customer_acc_no"));
                    detail.setRespomseCode(resultSet.getString("error_code"));
                    detail.setResponseDesc(resultSet.getString("error_message"));
                    detail.setCountryCode(resultSet.getString("country_code"));
                    detail.setAmount(resultSet.getString("amount"));
                    detail.setBeneficiaryAccNo(resultSet.getString("DESTINATION_ACCOUNT"));
                    detail.setDestinationCcy(resultSet.getString("DESTINATION_CCY"));
                    detail.setSendCCy(resultSet.getString("SENDER_CCY"));

                    response.add(detail);
                }

            }

            System.out.println("DAO Response >>>>>>>>>>>>>>>>>>>>>>> " + response);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection.closeDbConnection(connection, callableStatement, resultSet);
        }

        return response;
    }


    public List<MobileAppRespModel> getSearchDataBillPayments (String startDate, String endDate, BigDecimal recordLimit, String transactionRef,
                                                                BigDecimal amount, String customerAccNo, String beneficiaryAccNo, String affiliate){
        Connection connection = null;
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;

        String responseCode;
        String responseMessage;
        List<MobileAppRespModel> response = new ArrayList<>();
        MobileAppRespModel detail = null;

        try {
            connection = dataSource.getDataSource(oracleJndiKey).getConnection();
            String query = "{call eco_rpt_two_pkg.get_search_mobile_app_bp(?,?,?,?,?,?,?,?,?,?,?)}";
            callableStatement = connection.prepareCall(query);
            callableStatement.setString(1, startDate);
            callableStatement.setString(2, endDate);
            callableStatement.setBigDecimal(3, recordLimit);
            callableStatement.setString(4, transactionRef);
            callableStatement.setBigDecimal(5, amount);
            callableStatement.setString(6, customerAccNo);
            callableStatement.setString(7, beneficiaryAccNo);
            callableStatement.setString(8, affiliate);
            callableStatement.registerOutParameter(9, Types.VARCHAR);
            callableStatement.registerOutParameter(10, Types.VARCHAR);
            callableStatement.registerOutParameter(11, OracleTypes.CURSOR);
            callableStatement.execute();

            responseCode = callableStatement.getString(9);
            responseMessage = callableStatement.getString(10);

            LOGGER.debug("responseCode >>>>>>>>>>>>>>>>>>>>>>> " + responseCode);
            LOGGER.debug("responseMessage >>>>>>>>>>>>>>>>>>>>>>> " + responseMessage);

            resultSet = (ResultSet) callableStatement.getObject(11);

            if(responseCode.equals("000") && responseMessage.equals("SUCCESS")){

                while (resultSet.next()) {

                    detail = new MobileAppRespModel();

                    detail.setTransactionRef(resultSet.getString("transaction_ref"));
                    detail.setTranDateTime(resultSet.getString("trans_date_time"));
                    detail.setCustomerAccNo(resultSet.getString("customer_acc_no"));
                    detail.setRespomseCode(resultSet.getString("error_code"));
                    detail.setResponseDesc(resultSet.getString("error_message"));
                   // detail.setErrorCategory(resultSet.getString("error_category"));
                    detail.setCountryCode(resultSet.getString("country_code"));
                    detail.setAmount(resultSet.getString("amount"));
                    detail.setCcy(resultSet.getString("CCY"));
                    detail.setProductCode(resultSet.getString("PRODUCT_CODE"));

                    response.add(detail);
                }

            }

            System.out.println("DAO Response >>>>>>>>>>>>>>>>>>>>>>> " + response);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection.closeDbConnection(connection, callableStatement, resultSet);
        }

        return response;
    }

    public List<MobileAppRespModel> getSearchDataAirtimeTopUp (String startDate, String endDate, BigDecimal recordLimit, String transactionRef,
                                                               BigDecimal amount, String customerAccNo, String beneficiaryAccNo, String affiliate){
        Connection connection = null;
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;

        String responseCode;
        String responseMessage;
        List<MobileAppRespModel> response = new ArrayList<>();
        MobileAppRespModel detail = null;

        try {
            connection = dataSource.getDataSource(oracleJndiKey).getConnection();
            String query = "{call eco_rpt_two_pkg.get_search_mobile_app_at(?,?,?,?,?,?,?,?,?,?,?)}";
            callableStatement = connection.prepareCall(query);
            callableStatement.setString(1, startDate);
            callableStatement.setString(2, endDate);
            callableStatement.setBigDecimal(3, recordLimit);
            callableStatement.setString(4, transactionRef);
            callableStatement.setBigDecimal(5, amount);
            callableStatement.setString(6, customerAccNo);
            callableStatement.setString(7, beneficiaryAccNo);
            callableStatement.setString(8, affiliate);
            callableStatement.registerOutParameter(9, Types.VARCHAR);
            callableStatement.registerOutParameter(10, Types.VARCHAR);
            callableStatement.registerOutParameter(11, OracleTypes.CURSOR);
            callableStatement.execute();

            responseCode = callableStatement.getString(9);
            responseMessage = callableStatement.getString(10);

            LOGGER.debug("responseCode >>>>>>>>>>>>>>>>>>>>>>> " + responseCode);
            LOGGER.debug("responseMessage >>>>>>>>>>>>>>>>>>>>>>> " + responseMessage);

            resultSet = (ResultSet) callableStatement.getObject(11);

            if(responseCode.equals("000") && responseMessage.equals("SUCCESS")){

                while (resultSet.next()) {

                    detail = new MobileAppRespModel();

                    detail.setTransactionRef(resultSet.getString("transaction_ref"));
                    detail.setTranDateTime(resultSet.getString("trans_date_time"));
                    detail.setCustomerAccNo(resultSet.getString("customer_acc_no"));
                    detail.setRespomseCode(resultSet.getString("error_code"));
                    detail.setResponseDesc(resultSet.getString("error_message"));
                    //detail.setErrorCategory(resultSet.getString("error_category"));
                    detail.setCountryCode(resultSet.getString("country_code"));
                    detail.setAmount(resultSet.getString("amount"));
                    detail.setCcy(resultSet.getString("CCY"));
                    detail.setProductCode(resultSet.getString("PRODUCT_CODE"));

                    response.add(detail);
                }

            }

            System.out.println("DAO Response >>>>>>>>>>>>>>>>>>>>>>> " + response);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection.closeDbConnection(connection, callableStatement, resultSet);
        }

        return response;
    }

    public List<MobileAppRespModel> getSearchDataInterBankTransfer (String startDate, String endDate, BigDecimal recordLimit, String transactionRef,
                                                               BigDecimal amount, String customerAccNo, String beneficiaryAccNo, String affiliate){
        Connection connection = null;
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;

        String responseCode;
        String responseMessage;
        List<MobileAppRespModel> response = new ArrayList<>();
        MobileAppRespModel detail = null;

        try {
            connection = dataSource.getDataSource(oracleJndiKey).getConnection();
            String query = "{call eco_rpt_two_pkg.get_search_mobile_app_ibt(?,?,?,?,?,?,?,?,?,?,?)}";
            callableStatement = connection.prepareCall(query);
            callableStatement.setString(1, startDate);
            callableStatement.setString(2, endDate);
            callableStatement.setBigDecimal(3, recordLimit);
            callableStatement.setString(4, transactionRef);
            callableStatement.setBigDecimal(5, amount);
            callableStatement.setString(6, customerAccNo);
            callableStatement.setString(7, beneficiaryAccNo);
            callableStatement.setString(8, affiliate);
            callableStatement.registerOutParameter(9, Types.VARCHAR);
            callableStatement.registerOutParameter(10, Types.VARCHAR);
            callableStatement.registerOutParameter(11, OracleTypes.CURSOR);
            callableStatement.execute();

            responseCode = callableStatement.getString(9);
            responseMessage = callableStatement.getString(10);

            LOGGER.debug("responseCode >>>>>>>>>>>>>>>>>>>>>>> " + responseCode);
            LOGGER.debug("responseMessage >>>>>>>>>>>>>>>>>>>>>>> " + responseMessage);

            resultSet = (ResultSet) callableStatement.getObject(11);

            if(responseCode.equals("000") && responseMessage.equals("SUCCESS")){

                while (resultSet.next()) {

                    detail = new MobileAppRespModel();

                    detail.setTransactionRef(resultSet.getString("transaction_ref"));
                    detail.setTranDateTime(resultSet.getString("trans_date_time"));
                    detail.setCustomerAccNo(resultSet.getString("customer_acc_no"));
                    detail.setRespomseCode(resultSet.getString("error_code"));
                    detail.setResponseDesc(resultSet.getString("error_message"));
                   // detail.setErrorCategory(resultSet.getString("error_category"));
                    detail.setCountryCode(resultSet.getString("country_code"));
                    detail.setAmount(resultSet.getString("amount"));
                    detail.setBeneficiaryAccNo(resultSet.getString("BEN_ACCOUNT_NO"));
                    detail.setCcy(resultSet.getString("CCY"));
                    detail.setBeneficiaryBank(resultSet.getString("DESTINATION_BANK_CODE"));

                    response.add(detail);
                }

            }

            System.out.println("DAO Response >>>>>>>>>>>>>>>>>>>>>>> " + response);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection.closeDbConnection(connection, callableStatement, resultSet);
        }

        return response;
    }


    public List<MobileAppModalRespModel> getSearchDataInterAffTransferModal (String transactionRef){
        Connection connection = null;
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;

        String responseCode;
        String responseMessage;
        List<MobileAppModalRespModel> response = new ArrayList<>();
        MobileAppModalRespModel detail = null;

        try {
            connection = dataSource.getDataSource(oracleJndiKey).getConnection();
            //connection = cardOracleDataSource().getConnection();
            String query = "{call eco_rpt_two_pkg.get_search_data_modal_mb_iat(?,?,?,?)}";
            callableStatement = connection.prepareCall(query);
            callableStatement.setString(1, transactionRef);
            callableStatement.registerOutParameter(2, Types.VARCHAR);
            callableStatement.registerOutParameter(3, Types.VARCHAR);
            callableStatement.registerOutParameter(4, OracleTypes.CURSOR);
            callableStatement.execute();

            responseCode = callableStatement.getString(2);
            responseMessage = callableStatement.getString(3);

            LOGGER.debug("responseCode >>>>>>>>>>>>>>>>>>>>>>> " + responseCode);
            LOGGER.debug("responseMessage >>>>>>>>>>>>>>>>>>>>>>> " + responseMessage);

            resultSet = (ResultSet) callableStatement.getObject(4);

            if(responseCode.equals("000") && responseMessage.equals("SUCCESS")){

                while (resultSet.next()) {

                    detail = new MobileAppModalRespModel();

                    detail.setTransactionRef(resultSet.getString("transaction_ref"));
                    detail.setTransactionDateTime(resultSet.getString("trans_date_time"));
                    detail.setCustomerId(resultSet.getString("customer_id"));
                    detail.setCustomerName(resultSet.getString("customer_name"));
                    detail.setCustomerAccNo(resultSet.getString("customer_acc_no"));
                    detail.setTranCcy(resultSet.getString("trans_ccy"));
                    detail.setAmount(resultSet.getBigDecimal("amount"));
                    detail.setBeneficiaryAccNo(resultSet.getString("beneficiary_acc_no"));
                    detail.setDestinationBankCode(resultSet.getString("destination_bank_code"));
                    detail.setResponseCode(resultSet.getString("error_code"));

                    response.add(detail);
                }

            }

            System.out.println("DAO Response >>>>>>>>>>>>>>>>>>>>>>> " + response);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection.closeDbConnection(connection, callableStatement, resultSet);
        }

        return response;
    }



    public List<MobileAppModalRespModel> getSearchDataBillPaymentModal (String transactionRef){
        Connection connection = null;
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;

        String responseCode;
        String responseMessage;
        List<MobileAppModalRespModel> response = new ArrayList<>();
        MobileAppModalRespModel detail = null;

        try {
            connection = dataSource.getDataSource(oracleJndiKey).getConnection();
            //connection = cardOracleDataSource().getConnection();
            String query = "{call eco_rpt_two_pkg.get_search_data_modal_mb_bp(?,?,?,?)}";
            callableStatement = connection.prepareCall(query);
            callableStatement.setString(1, transactionRef);
            callableStatement.registerOutParameter(2, Types.VARCHAR);
            callableStatement.registerOutParameter(3, Types.VARCHAR);
            callableStatement.registerOutParameter(4, OracleTypes.CURSOR);
            callableStatement.execute();

            responseCode = callableStatement.getString(2);
            responseMessage = callableStatement.getString(3);

            LOGGER.debug("responseCode >>>>>>>>>>>>>>>>>>>>>>> " + responseCode);
            LOGGER.debug("responseMessage >>>>>>>>>>>>>>>>>>>>>>> " + responseMessage);

            resultSet = (ResultSet) callableStatement.getObject(4);

            if(responseCode.equals("000") && responseMessage.equals("SUCCESS")){

                while (resultSet.next()) {

                    detail = new MobileAppModalRespModel();

                    detail.setTransactionRef(resultSet.getString("transaction_ref"));
                    detail.setTransactionDateTime(resultSet.getString("trans_date_time"));
                    detail.setProductCode(resultSet.getString("product_code"));
                    detail.setCustomerAccNo(resultSet.getString("customer_acc_no"));
                    detail.setDebitAccount(resultSet.getString("dr_account"));
                    detail.setTranCcy(resultSet.getString("trans_ccy"));
                    detail.setAmount(resultSet.getBigDecimal("amount"));
                    detail.setBillerCode(resultSet.getString("biller_code"));
                    detail.setResponseCode(resultSet.getString("error_code"));


                    response.add(detail);
                }

            }

            System.out.println("DAO Response >>>>>>>>>>>>>>>>>>>>>>> " + response);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection.closeDbConnection(connection, callableStatement, resultSet);
        }

        return response;
    }


    public List<MobileAppModalRespModel> getSearchDataAirtimeTopUpModal (String transactionRef){
        Connection connection = null;
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;

        String responseCode;
        String responseMessage;
        List<MobileAppModalRespModel> response = new ArrayList<>();
        MobileAppModalRespModel detail = null;

        try {
            connection = dataSource.getDataSource(oracleJndiKey).getConnection();
            //connection = cardOracleDataSource().getConnection();
            String query = "{call eco_rpt_two_pkg.get_search_data_modal_mb_at(?,?,?,?)}";
            callableStatement = connection.prepareCall(query);
            callableStatement.setString(1, transactionRef);
            callableStatement.registerOutParameter(2, Types.VARCHAR);
            callableStatement.registerOutParameter(3, Types.VARCHAR);
            callableStatement.registerOutParameter(4, OracleTypes.CURSOR);
            callableStatement.execute();

            responseCode = callableStatement.getString(2);
            responseMessage = callableStatement.getString(3);

            LOGGER.debug("responseCode >>>>>>>>>>>>>>>>>>>>>>> " + responseCode);
            LOGGER.debug("responseMessage >>>>>>>>>>>>>>>>>>>>>>> " + responseMessage);

            resultSet = (ResultSet) callableStatement.getObject(4);

            if(responseCode.equals("000") && responseMessage.equals("SUCCESS")){

                while (resultSet.next()) {

                    detail = new MobileAppModalRespModel();

                    detail.setTransactionRef(resultSet.getString("transaction_ref"));
                    detail.setTransactionDateTime(resultSet.getString("trans_date_time"));
                    detail.setProductCode(resultSet.getString("product_code"));
                    detail.setCustomerAccNo(resultSet.getString("customer_acc_no"));
                    detail.setDebitAccount(resultSet.getString("dr_account"));
                    detail.setTranCcy(resultSet.getString("trans_ccy"));
                    detail.setAmount(resultSet.getBigDecimal("amount"));
                    detail.setBillerCode(resultSet.getString("biller_code"));
                    detail.setResponseCode(resultSet.getString("error_code"));
//                    detail.setErrorDesc(resultSet.getString("error_description"));
//                    detail.setProposedResolution(resultSet.getString("proposed_resolution"));

                    response.add(detail);
                }

            }

            System.out.println("DAO Response >>>>>>>>>>>>>>>>>>>>>>> " + response);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection.closeDbConnection(connection, callableStatement, resultSet);
        }

        return response;
    }

    public List<MobileAppModalRespModel> getSearchDataInterBankTransferModal (String transactionRef){
        Connection connection = null;
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;

        String responseCode;
        String responseMessage;
        List<MobileAppModalRespModel> response = new ArrayList<>();
        MobileAppModalRespModel detail = null;

        try {
            connection = (Connection) dataSource.getDataSource(oracleJndiKey);
            //connection = cardOracleDataSource().getConnection();
            String query = "{call eco_rpt_two_pkg.get_search_data_modal_mb_ibt(?,?,?,?)}";
            callableStatement = connection.prepareCall(query);
            callableStatement.setString(1, transactionRef);
            callableStatement.registerOutParameter(2, Types.VARCHAR);
            callableStatement.registerOutParameter(3, Types.VARCHAR);
            callableStatement.registerOutParameter(4, OracleTypes.CURSOR);
            callableStatement.execute();

            responseCode = callableStatement.getString(2);
            responseMessage = callableStatement.getString(3);

            LOGGER.debug("responseCode >>>>>>>>>>>>>>>>>>>>>>> " + responseCode);
            LOGGER.debug("responseMessage >>>>>>>>>>>>>>>>>>>>>>> " + responseMessage);

            resultSet = (ResultSet) callableStatement.getObject(4);

            if(responseCode.equals("000") && responseMessage.equals("SUCCESS")){

                while (resultSet.next()) {

                    detail = new MobileAppModalRespModel();

                    detail.setTransactionRef(resultSet.getString("transaction_ref"));
                    detail.setTransactionDateTime(resultSet.getString("trans_date_time"));
                    detail.setCustomerId(resultSet.getString("customer_id"));
                    detail.setCustomerName(resultSet.getString("customer_name"));
                    detail.setCustomerAccNo(resultSet.getString("customer_acc_no"));
                    detail.setCustomnerPhoneNumber(resultSet.getString("customer_phone_no"));
                    detail.setTranCcy(resultSet.getString("trans_ccy"));
                    detail.setAmount(resultSet.getBigDecimal("amount"));
                    detail.setBeneficiaryAccNo(resultSet.getString("beneficiary_acc_no"));
                    detail.setDestinationBankCode(resultSet.getString("destination_bank_code"));
                    detail.setResponseCode(resultSet.getString("error_code"));
                    detail.setSessionId(resultSet.getString("session_id"));

                    response.add(detail);
                }

            }

            System.out.println("DAO Response >>>>>>>>>>>>>>>>>>>>>>> " + response);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection.closeDbConnection(connection, callableStatement, resultSet);
        }

        return response;
    }




}
