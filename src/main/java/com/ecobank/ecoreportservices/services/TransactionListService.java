package com.ecobank.ecoreportservices.services;

import com.ecobank.ecoreportservices.constants.EcobankOnlineSubServices;
import com.ecobank.ecoreportservices.constants.MobileAppSubServices;
import com.ecobank.ecoreportservices.constants.ResponseEnum;
import com.ecobank.ecoreportservices.daos.cards.CardServicesDaoImpl;
import com.ecobank.ecoreportservices.daos.ecobankonline.EcobankOnlineDaoImpl;
import com.ecobank.ecoreportservices.daos.mobileapp.MobileAppServicesDaoImpl;
import com.ecobank.ecoreportservices.models.*;
import com.ecobank.ecoreportservices.utils.DateValidation;
import com.ecobank.ecoreportservices.utils.HelperMethods;
import com.ecobank.ecoreportservices.utils.ResponseUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


@Service
@Slf4j
public class TransactionListService {

    @Autowired
    CardServicesDaoImpl cardServicesDao;

    @Autowired
    EcobankOnlineDaoImpl ecobankOnlineDao;

    @Autowired
    HelperMethods helperMethods;

    @Autowired
    MobileAppServicesDaoImpl mobileAppServicesDao;

    @Autowired
    ResponseUtils responseUtils;


    /**
     * Returns all transaction on a source account for a date range
     * @param transactionList
     * @param keys
     * @return
     * @throws Exception
     */
    public ResponseEntity<?> checkForServicesAndCallServiceMethod(TransactionList transactionList, AesKeyPair keys) throws Exception {

        if (DateValidation.isValidDate(transactionList.getStartDate(), transactionList.getEndDate())) {
            return responseUtils.getError(ResponseEnum.INVALID_DATE, keys);
        }

        Response response = new Response();
        if (transactionList.getServiceCode().equals("Cards")) {
            CardsSearchDataRequest request = new CardsSearchDataRequest();
            request.setCluster(transactionList.getServiceCode());
            request.setStartDate(transactionList.getStartDate());
            request.setEndDate(transactionList.getEndDate());
            request.setRecordLimit(transactionList.getRecordLimit());
            request.setCardAccount(transactionList.getSourceAccount());
            request.setAmount(transactionList.getAmount());
            request.setTransactionRefNumber(transactionList.getTransactionReference() );
                log.info("transaction list: {}", Collections.singletonList(cardServicesDao.getSearchDataCards(request)).toString());
                return responseUtils.getSuccess(ResponseEnum.SUCCESS, Collections.singletonList(cardServicesDao.getSearchDataCards(request)), keys);
        }
        else if (transactionList.getServiceCode().equals("EONLINE") && EcobankOnlineSubServices.contains(transactionList.getSubServiceCode())) {
                String subService = EcobankOnlineSubServices.getEnumByString(transactionList.getSubServiceCode());
                String amountValue = transactionList.getAmount() == null? "" : transactionList.getAmount().toString();
                log.info("transactionList: {}", Collections.singletonList(ecobankOnlineDao.getEcobankOnlineTransactions(transactionList.getTransactionReference()
                        , transactionList.getStartDate(), transactionList.getEndDate(), transactionList.getSourceAccount(), subService, amountValue,
                        transactionList.getBeneficiaryAccount(), transactionList.getAffiliate(), transactionList.getRecordLimit())).toString());
            return responseUtils.getSuccess(ResponseEnum.SUCCESS, Collections.singletonList(ecobankOnlineDao.getEcobankOnlineTransactions(transactionList.getTransactionReference()
                    , transactionList.getStartDate(), transactionList.getEndDate(), transactionList.getSourceAccount(), subService, amountValue,
                    transactionList.getBeneficiaryAccount(), transactionList.getAffiliate(), transactionList.getRecordLimit())), keys);
        }

        else if (transactionList.getServiceCode().equals("MAPP") && MobileAppSubServices.contains(transactionList.getSubServiceCode())) {
            log.info("transaction list: {}", Collections.singletonList(callMobileAppTransactionMethod(transactionList.getSubServiceCode(), transactionList.getStartDate(), transactionList.getEndDate(), transactionList.getRecordLimit(),
                    transactionList.getTransactionReference(), transactionList.getAmount(), transactionList.getSourceAccount(), transactionList.getBeneficiaryAccount(), transactionList.getAffiliate())).toString());
            return responseUtils.getSuccess(ResponseEnum.SUCCESS, Collections.singletonList(callMobileAppTransactionMethod(transactionList.getSubServiceCode(), transactionList.getStartDate(), transactionList.getEndDate(), transactionList.getRecordLimit(),
                    transactionList.getTransactionReference(), transactionList.getAmount(), transactionList.getSourceAccount(), transactionList.getBeneficiaryAccount(), transactionList.getAffiliate())), keys);
        }
        log.info("sub service name :{}", responseUtils.getError(ResponseEnum.NOT_EXIST, keys));
       return responseUtils.getError(ResponseEnum.NOT_EXIST, keys);
    }

    /**
     * Use a switch case to determine method call from Mobile DAO
     * @param subServiceName
     * @param startDate
     * @param endDate
     * @param recordLimit
     * @param sourceAccount
     * @param beneficiaryAccount
     * @param affiliate
     * @param transactionReference
     * @param amount
     * @return a List
     */

    public List<?> callMobileAppTransactionMethod(String subServiceName, String startDate, String endDate,
                                                  int recordLimit, String transactionReference, BigDecimal amount,String sourceAccount,
                                                  String beneficiaryAccount, String affiliate) {

        switch (subServiceName) {
            case "Inter Bank":
                return mobileAppServicesDao.getSearchDataInterBankTransfer(startDate, endDate, BigDecimal.valueOf(recordLimit),
                        transactionReference, amount, sourceAccount, beneficiaryAccount, affiliate);
            case "Inter Affiliate Transfer":
                return mobileAppServicesDao.getSearchTransDataInterAff(startDate, endDate,  BigDecimal.valueOf(recordLimit),
                        transactionReference, amount, sourceAccount, beneficiaryAccount, affiliate);
            case "Bills Payment Mobile":
                return mobileAppServicesDao.getSearchDataBillPayments(startDate, endDate, BigDecimal.valueOf(recordLimit),
                        transactionReference, amount, sourceAccount, beneficiaryAccount, affiliate);
            case "International Transfer":
                return mobileAppServicesDao.getSearchTransDataInternationalTrans(startDate, endDate, BigDecimal.valueOf(recordLimit),
                        transactionReference, amount, sourceAccount, beneficiaryAccount, affiliate);
        }

        // TODO : Replace with Domestic Transfer logic when available
        return mobileAppServicesDao.getSearchTransDataInternationalTrans(startDate, endDate, BigDecimal.valueOf(recordLimit),
                transactionReference, amount, sourceAccount, beneficiaryAccount, affiliate);

    }


    /**
     * Uses reflection to call method dynamically at runtime
     * Not in use.
     * @param subServiceName
     * @param startDate
     * @param endDate
     * @param recordLimit
     * @param sourceAccount
     * @param beneficiaryAccount
     * @param affiliate
     * @param transactionReference
     * @param amount
     * @return
     * @throws ClassNotFoundException
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     */

    public List<?> callMobileMethodByReflection(
            String subServiceName, String startDate, String endDate,
            String recordLimit, String sourceAccount, String beneficiaryAccount,
            String affiliate, String transactionReference, String amount
    ) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        String mobileDAO = "com.ecobank.report.daoimplementation.mobileappimpl.MobileAppServicesDaoImpl";
        Class<?> mobileDAOClass = Class.forName(mobileDAO);
        Object mobile = mobileDAOClass.newInstance();
        System.out.println(mobile + "mobileObject");
        BigDecimal limit = recordLimit == null ? null : new BigDecimal(recordLimit);
        BigDecimal amountBig = amount == null ? null : new BigDecimal(amount);
        String methodName = helperMethods.getMobileServicesMethodName(subServiceName);
        System.out.println(methodName + "methodNamee");
        Class<?>[] paramTypes = {String.class, String.class, BigDecimal.class, String.class, BigDecimal.class, String.class, String.class, String.class};
        Method returnMethodResult = mobile.getClass().getMethod(methodName, paramTypes);
        System.out.println(returnMethodResult + "methods");
        return (List<?>) returnMethodResult.invoke(mobile, startDate, endDate, limit, transactionReference, amountBig,
                sourceAccount, beneficiaryAccount, affiliate);
    }








}
