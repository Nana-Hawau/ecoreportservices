package com.ecobank.ecoreportservices.services;

import com.ecobank.ecoreportservices.constants.*;
import com.ecobank.ecoreportservices.entities.ServiceInformation;
import com.ecobank.ecoreportservices.entities.SubServices;
import com.ecobank.ecoreportservices.models.AesKeyPair;
import com.ecobank.ecoreportservices.utils.ResponseUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import java.util.Arrays;
import java.util.List;

@Service
@Slf4j
public class ServiceInformationFactory {

    @Autowired
    ResponseUtils responseUtils;

    public ResponseEntity<?> build (AesKeyPair keys) throws Exception {
        log.info("service information : {} ", Arrays.asList(buildCards(),buildEcoBankOnline(),buildMobileApp()).toString());
        return responseUtils.getSuccess(ResponseEnum.SUCCESS, Arrays.asList(buildCards(),buildEcoBankOnline(),buildMobileApp()), keys);
    }


    public ServiceInformation buildEcoBankOnline () {
        SubServices subServiceBill = new SubServices(EcobankOnlineSubServices.BILL.getSubServiceName(), EcobankOnlineSubServices.BILL.getSubServiceCode());
        SubServices subServiceBillPay = new SubServices(EcobankOnlineSubServices.BILL_PAY.getSubServiceName(), EcobankOnlineSubServices.BILL_PAY.getSubServiceCode());
        SubServices subServiceInternationalSwiftTransfer = new SubServices(EcobankOnlineSubServices.InternationalSwiftTransfer.getSubServiceName(), EcobankOnlineSubServices.InternationalSwiftTransfer.getSubServiceCode());
        SubServices subServiceNipPay = new SubServices(EcobankOnlineSubServices.NIP_PAY.getSubServiceName(), EcobankOnlineSubServices.NIP_PAY.getSubServiceCode());
        SubServices subServiceDomesticTransfer = new SubServices(EcobankOnlineSubServices.processDomesticTransfer.getSubServiceName(), EcobankOnlineSubServices.processDomesticTransfer.getSubServiceCode());
        SubServices subServiceInternalTransfer = new SubServices(EcobankOnlineSubServices.processInternalTransfer.getSubServiceName(), EcobankOnlineSubServices.processInternalTransfer.getSubServiceCode());
        SubServices subServiceSelfTransfer = new SubServices(EcobankOnlineSubServices.processSelfTransfer.getSubServiceName(), EcobankOnlineSubServices.processSelfTransfer.getSubServiceCode());
        SubServices subServiceRTSendMoney = new SubServices(EcobankOnlineSubServices.RTSendMoney.getSubServiceName(), EcobankOnlineSubServices.RTSendMoney.getSubServiceCode());
        SubServices subServiceInternationalTransfer = new SubServices(EcobankOnlineSubServices.processInternationalTransfer.getSubServiceName(), EcobankOnlineSubServices.processInternationalTransfer.getSubServiceCode());

        List<SubServices> SubServiceEcobankOnlineList = Arrays.asList(subServiceBill, subServiceBillPay, subServiceDomesticTransfer,
                subServiceInternalTransfer, subServiceDomesticTransfer, subServiceInternationalSwiftTransfer, subServiceNipPay
        ,subServiceRTSendMoney, subServiceSelfTransfer, subServiceInternationalTransfer);
        return new ServiceInformation(Services.ECOBANK_ONLINE.getServiceName(), Services.ECOBANK_ONLINE.getServiceCode(),SubServiceEcobankOnlineList);
    }

    public ServiceInformation buildCards () {
        SubServices subServiceENG = new SubServices(CardsClusters.ENG.getSubServiceName(), CardsClusters.ENG.getSubServiceCode());
        SubServices subServiceCESA1 = new SubServices(CardsClusters.CESA1.getSubServiceName(), CardsClusters.CESA1.getSubServiceCode());
        SubServices subServiceCESA2 = new SubServices(CardsClusters.CESA2.getSubServiceName(), CardsClusters.CESA2.getSubServiceCode());
        SubServices subServiceAWA = new SubServices(CardsClusters.AWA.getSubServiceName(), CardsClusters.AWA.getSubServiceCode());
        SubServices subServiceFWA = new SubServices(CardsClusters.FWA.getSubServiceName(), CardsClusters.FWA.getSubServiceCode());

        List<SubServices> cardsSubServicesList = Arrays.asList(subServiceAWA, subServiceFWA, subServiceCESA1, subServiceCESA2, subServiceENG);
        return new ServiceInformation(Services.CARDS.getServiceName(), Services.CARDS.getServiceName(), cardsSubServicesList);
    }

    public ServiceInformation buildMobileApp () {
        SubServices subServiceBillsPayment = new SubServices(MobileAppSubServices.BILLS_PAYMENT.getSubServiceName(), MobileAppSubServices.BILLS_PAYMENT.getSubServiceCode());
        SubServices subServiceDomesticTransfer = new SubServices(MobileAppSubServices.DOMESTIC_TRANSFER.getSubServiceName(), MobileAppSubServices.DOMESTIC_TRANSFER.getSubServiceCode());
        SubServices subServiceInterAffiliate = new SubServices(MobileAppSubServices.INTER_AFF.getSubServiceName(), MobileAppSubServices.INTER_AFF.getSubServiceCode());
        SubServices subServiceInterBank = new SubServices(MobileAppSubServices.INTER_BANK.getSubServiceName(), MobileAppSubServices.INTER_BANK.getSubServiceCode());
        SubServices subServiceIntlTransfer = new SubServices(MobileAppSubServices.INTL_TRANSFER.getSubServiceName(), MobileAppSubServices.INTL_TRANSFER.getSubServiceCode());

        //TODO: Add Domestic Transfer and Intl Transfer when they are available;

        List<SubServices> mobileAppSubServicesList = Arrays.asList(subServiceBillsPayment, subServiceInterAffiliate,
                subServiceInterBank);
        return new ServiceInformation(Services.MOBILE_APP.getServiceName(),Services.MOBILE_APP.getServiceCode() , mobileAppSubServicesList);
    }



}
