package com.ecobank.ecoreportservices.services;

import com.ecobank.ecoreportservices.constants.ErrorMappingServiceTypes;
import com.ecobank.ecoreportservices.constants.ResponseEnum;
import com.ecobank.ecoreportservices.models.*;
import com.ecobank.ecoreportservices.utils.MergeObjects;
import com.ecobank.ecoreportservices.utils.ResponseUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class MergeDetailsService {

    @Autowired
    AccountInfoEnquiryService accountInfoEnquiryService;
    @Autowired
    TransactionDetailsService transactionDetailsService;

    @Autowired
    ResponseUtils responseUtils;


    public <T> ResponseEntity<T> mergeDetails (TransactionDetailsRequest transactionDetailsRequest, AesKeyPair aesKeyPair) throws Exception {

        if (ErrorMappingServiceTypes.contains(transactionDetailsRequest.getSubServiceCode())) {
            String subService = ErrorMappingServiceTypes.getString(transactionDetailsRequest.getSubServiceCode()).equals("Cards")? "Cards" : ErrorMappingServiceTypes.getEnumByString(transactionDetailsRequest.getSubServiceCode());
            AccountEnquiry accountEnquiry = new AccountEnquiry(transactionDetailsRequest.getAccountNumber(), transactionDetailsRequest.getAffiliate());
            AccountDetailInfoResponse accountEnquiryResponse = accountInfoEnquiryService.getAccountInfo(accountEnquiry);
            AccountDetails accountDetails = extractRelevantAccountDetails(accountEnquiryResponse);
            Object transactionDetailsResponse = transactionDetailsService.modal(subService, transactionDetailsRequest.getTransactionReference());
            Object mappedErrors = transactionDetailsService.mappedErrors(subService, transactionDetailsRequest.getTransactionReference());
            Object mergedObject = MergeObjects.mergeObjects(accountDetails, transactionDetailsResponse, mappedErrors);
            return (ResponseEntity<T>) responseUtils.getSuccess(ResponseEnum.SUCCESS, mergedObject);
        }

        return (ResponseEntity<T>) responseUtils.getError(ResponseEnum.NOT_EXIST, aesKeyPair);



    }

    public AccountDetails extractRelevantAccountDetails (AccountDetailInfoResponse accountEnquiryResponse) {
        JSONObject udfData = new JSONObject(accountEnquiryResponse).getJSONArray("UDFData").getJSONObject(17);
        String customerCategory = new UDFData(udfData).getUdfValue();
        String email = accountEnquiryResponse.getEmailID();
        String phoneNumber = accountEnquiryResponse.getPhoneNo();
        String accountName = accountEnquiryResponse.getAccountName();
        return new AccountDetails(accountName, email, phoneNumber, customerCategory);

    }
}
