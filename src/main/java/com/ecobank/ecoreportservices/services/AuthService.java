package com.ecobank.ecoreportservices.services;


import com.ecobank.ecoreportservices.constants.ResponseEnum;
import com.ecobank.ecoreportservices.models.ResponseAuth;
import com.ecobank.ecoreportservices.models.Token;
import com.ecobank.ecoreportservices.security.jwt.JwtUtils;
import com.ecobank.ecoreportservices.utils.ResponseUtils;
import com.ecobank.ecoreportservices.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
public class AuthService {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    SecurityUtils securityUtils;

    @Autowired
    ResponseUtils responseUtils;



    public ResponseEntity<ResponseAuth> getAccessToken(String clientId, String clientSecret, String sourceCode) throws Exception {
        Optional<Token> token;
        securityUtils.doAuth(clientId, clientSecret, sourceCode);
        token = Optional.of(jwtUtils.createToken(clientId));
        return responseUtils.getSuccess(ResponseEnum.SUCCESS, token.get());
    }



    public void authenticate(String clientId, String clientSecret) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(clientId, clientSecret));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }


}
