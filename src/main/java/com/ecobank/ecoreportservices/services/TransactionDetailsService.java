package com.ecobank.ecoreportservices.services;

import com.ecobank.ecoreportservices.constants.EcobankOnlineSubServices;
import com.ecobank.ecoreportservices.constants.ErrorMappingServiceTypes;
import com.ecobank.ecoreportservices.constants.ResponseEnum;
import com.ecobank.ecoreportservices.daos.ErrorMapperDaoImplementation;
import com.ecobank.ecoreportservices.daos.ecobankonline.EcobankOnlineDaoImpl;
import com.ecobank.ecoreportservices.daos.mobileapp.MobileAppServicesDaoImpl;
import com.ecobank.ecoreportservices.models.MappedErrorRequest;
import com.ecobank.ecoreportservices.models.TransactionDetailsRequest;
import com.ecobank.ecoreportservices.utils.ResponseUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Collections;
import java.util.List;

@Service
@Slf4j
public class TransactionDetailsService {

    @Autowired
    MobileAppServicesDaoImpl mobileAppServicesDao;

    @Autowired
    EcobankOnlineDaoImpl ecobankOnlineDao;

    @Autowired
    ErrorMapperDaoImplementation errorMapperDao;

    @Autowired
    ResponseUtils responseUtils;

    public Object modal (String subServiceCode, String transactionReference) throws Exception {
            List <?> result = callModalMethodsForServices(subServiceCode, transactionReference);
            log.info("Modal {} :", Collections.singletonList(result));
            return Collections.singletonList(result).toString();
    }


    public Object mappedErrors (String subServiceCode, String errorCode) throws Exception {
        log.info("mapped error requests: {}", subServiceCode + errorCode);
            log.info("Mapped Errors: {}", Collections.singletonList(errorMapperDao.getMappedErrorFields(subServiceCode, errorCode)).toString());
            return Collections.singletonList(errorMapperDao.getMappedErrorFields(subServiceCode, errorCode)).toString();
    }



    public List<?> callModalMethodsForServices (String subServiceName, String transactionReference) {
        switch (subServiceName) {
            case "Inter Bank":
                return mobileAppServicesDao.getSearchDataInterBankTransferModal(transactionReference);
            case "Inter Affiliate Transfer":
                return mobileAppServicesDao.getSearchDataInterAffTransferModal(transactionReference);
            case "Bills Payment Mobile":
                return mobileAppServicesDao.getSearchDataBillPaymentModal(transactionReference);
            case "International Transfer":
                return  Collections.singletonList(ecobankOnlineDao.getNIPPAYTransactionModal(transactionReference, EcobankOnlineSubServices.getEnumByString("International Transfer")));
            case "International Swift Transfer":
                return  Collections.singletonList(ecobankOnlineDao.getNIPPAYTransactionModal(transactionReference, EcobankOnlineSubServices.getEnumByString("International Swift Transfer")));
            case "Domestic Transfer":
                return  Collections.singletonList(ecobankOnlineDao.getNIPPAYTransactionModal(transactionReference, EcobankOnlineSubServices.getEnumByString("Domestic Transfer")));
            case "Bills Payment Across Group":
                return  Collections.singletonList(ecobankOnlineDao.getBillTransactionModal(transactionReference, EcobankOnlineSubServices.getEnumByString("Bills Payment Across Group")));
            case "Self Transfer":
                return  Collections.singletonList(ecobankOnlineDao.getNIPPAYTransactionModal(transactionReference, EcobankOnlineSubServices.getEnumByString("Self Transfer")));
            case "NIBSS Payment":
                return  Collections.singletonList(ecobankOnlineDao.getNIPPAYTransactionModal(transactionReference, EcobankOnlineSubServices.getEnumByString("NIBSS Payment")));
            case "Rapid Transfer":
                return Collections.singletonList(ecobankOnlineDao.getRTTransactionModal(transactionReference, EcobankOnlineSubServices.getEnumByString("Rapid Transfer")));
            case "Internal Transfer":
                return Collections.singletonList(ecobankOnlineDao.getNIPPAYTransactionModal(
                        transactionReference, EcobankOnlineSubServices.getEnumByString("Internal Transfer")));
            case "Bills Payments":
                System.out.println(EcobankOnlineSubServices.getEnumByString("Bills Payments") + "Bills Payments");
                return Collections.singletonList(ecobankOnlineDao.getBillTransactionModal(transactionReference,
                        EcobankOnlineSubServices.getEnumByString("Bills Payments")));
        }

        // TODO : Replace with Domestic Transfer, Intl Transfer logic when available
        return mobileAppServicesDao.getSearchDataBillPaymentModal(transactionReference);

    }


}
