package com.ecobank.ecoreportservices.services;

import com.ecobank.ecoreportservices.constants.ResponseEnum;
import com.ecobank.ecoreportservices.models.*;
import com.ecobank.ecoreportservices.utils.ObjectConverter;
import com.ecobank.ecoreportservices.utils.ResponseUtils;
import com.ecobank.ecoreportservices.utils.RestClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;


@Service
@Slf4j
public class AccountInfoEnquiryService {
    @Autowired
    RestClient restClient;

    @Autowired
    ResponseUtils responseUtils;

    @Autowired
    Environment env;

    public AccountDetailInfoResponse getAccountInfo (AccountEnquiry accountEnquiry) throws Exception {
        HostHeaderInfo headerInfo = new HostHeaderInfo();
        headerInfo.setAffiliateCode(accountEnquiry.getAffiliate());
        headerInfo.setRequestId("SMTEGHDTXGGJLKVOJAJ7");
        headerInfo.setRequestToken("Ef2eZNCF+5qwScKlB6dJEjo2xZYquEVPi9ICUxR8VYI=");

        AccountDetailInfoResponse accountInfo = new AccountDetailInfoResponse();



        headerInfo.setRequestToken(headerInfo.getRequestToken() == null
                ? "03b7f1593dafecb7be7bd58e9e4bd1b1cd1a3d759f95ede1795b4bd0d645d3fa2482329fb101fd770b93d36a6ca394dbc0d5d5b0bb228eda57e56c017020141f"
                : headerInfo.getRequestToken());
        String response = "";

        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + "<AccountDetailInfoRequest>"
                + "    <hostHeaderInfo>" + "        <sourceCode>ECOMOBILE</sourceCode>" + "        <requestId>"
                + headerInfo.getRequestId() + "</requestId>" + "        <requestToken>" + headerInfo.getRequestToken()
                + "</requestToken>" + "        <requestType>GETACCINFO</requestType>" + "        <affiliateCode>"
                + headerInfo.getAffiliateCode() + "</affiliateCode>" + "		   <ipAddress>1.1.1.1</ipAddress>"
                + "        <sourceChannelId/>" + "    </hostHeaderInfo>" + "    <accountNo>" + accountEnquiry.getAccountNumber()
                + "</accountNo>" + "</AccountDetailInfoRequest>";


            response = restClient.post(env.getProperty("account.url"), xml);

        if (response != null) {
            String xmlResponse = response.split("<responseCode>")[1].split("</responseCode>")[0];
            if ("000".equals(xmlResponse)) {
                xmlResponse = response.substring(response.indexOf("<AccountDetailInfo>"),
                        response.indexOf("</AccountDetailInfoResponse>"));
                try {
                    accountInfo = (AccountDetailInfoResponse) ObjectConverter.marshalFromXML(xmlResponse,
                            AccountDetailInfoResponse.class);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        log.info("Modal {} :", accountInfo.toString());
        return accountInfo;
    }
}
