package com.ecobank.ecoreportservices.services;

import com.ecobank.ecoreportservices.constants.ResponseEnum;
import com.ecobank.ecoreportservices.daos.AffiliateListDaoImplementation;
import com.ecobank.ecoreportservices.models.AesKeyPair;
import com.ecobank.ecoreportservices.utils.ResponseUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class AffiliateService {

    @Autowired
    AffiliateListDaoImplementation affiliateListDaoImplementation;

    @Autowired
    ResponseUtils responseUtils;

    public ResponseEntity<?> affiliate (AesKeyPair keyPair) throws Exception {
        log.info("affiliates: {}", affiliateListDaoImplementation.getAffiliateList());
        return responseUtils.getSuccess(ResponseEnum.SUCCESS, affiliateListDaoImplementation.getAffiliateList(), keyPair);
    }



}
