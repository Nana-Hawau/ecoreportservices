package com.ecobank.ecoreportservices.services;

import com.ecobank.ecoreportservices.entities.ServiceInformation;
import com.ecobank.ecoreportservices.models.*;
import com.ecobank.ecoreportservices.utils.SecurityUtils;
import com.ecobank.ecoreportservices.utils.encryptionutils.RsaAesOfbStandard;
import com.ecobank.ecoreportservices.utils.encryptionutils.RsaAesStandard;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.JSONPObject;
import org.json.JSONObject;
import org.springframework.beans.factory.Aware;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

public class TestService {

    private final RsaAesStandard rsaUtil;

    public TestService(RsaAesStandard rsaUtil) {
        super();
        this.rsaUtil = rsaUtil;
    }



    public <T> String encryptResponse (T response, AesKeyPair keyPair) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        String res = objectMapper.writeValueAsString(response);
        String aeskey = rsaUtil.getSecretAESKeyAsString();
        String encryptedText = rsaUtil.encryptTextUsingAES(res, aeskey);
        System.out.println(encryptedText + ":::::encryptedText");
        String encryptedAESKeyString = rsaUtil.encryptAESKey(aeskey, keyPair.getPublicKey());
        System.out.println(encryptedAESKeyString + ":::::::encryptedAESKeyString");
        String encryptedRes =  encryptedText + "," + encryptedAESKeyString;
        return encryptedRes;
    }


    public <T> T decryptRequest(String request, AesKeyPair key, String encryptedAesKey, Class<T> elementClass) throws Exception {
        SecurityUtils securityUtils = new SecurityUtils(new RsaAesOfbStandard());
        return securityUtils.decryptRequest(request, key, encryptedAesKey, elementClass);

    }

    public static void main(String[] args) throws Exception {
//
//        "serviceName" : "Ecobank Online",
//                "subServiceName": "Rapid Transfer",
//                "startDate" : "10-07-2019",
//                "endDate": "10-03-2021",
//                "recordLimit": 5,
//                "sourceAccount": "1042020895",
//                "beneficiaryAccount": "",
//                "affiliate": "",
//                "transactionReference": "",
//                "amount" : ""



//        {
//            "serviceName" : "Cards",
//                "subServiceName": "ENG",
//                "startDate" : "10-07-2019",
//                "endDate": "10-03-2021",
//                "recordLimit": 5,
//                "sourceAccount": "5703070678",
//                "beneficiaryAccount": "",
//                "affiliate": "",
//                "transactionReference": "",
//                "amount" : ""
//        }

//        {
//            "subServiceName": "Rapid Transfer",
//                "transactionReference": "21238179526080"
//        }

//        {
//            "accountNumber": "5703070678",
//                "affiliate": "ENG"
//        }

//       AccountEnquiry accountEnquiry = new AccountEnquiry();
//       accountEnquiry.setAccountNumber("5703070678");
//       accountEnquiry.setAffiliate("ENG");
//
//        TransactionDetailsRequest detailsRequest = new TransactionDetailsRequest();;
//        detailsRequest.setSubServiceName("Rapid Transfer");
//        detailsRequest.setTransactionReference("21238179526080");
//
//        TransactionList transactionList = new TransactionList();
//        transactionList.setServiceName("Ecobank Online");
//        transactionList.setSubServiceName("Rapid Transfer");
//        transactionList.setStartDate("10-07-2019");
//        transactionList.setEndDate("10-03-2021");
//        transactionList.setRecordLimit(5);
//        transactionList.setSourceAccount("1042020895");

        TestService test = new TestService(new RsaAesOfbStandard());

        AesKeyPair pair = new AesKeyPair();
        pair.setPrivateKey("MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDD2Hjasb2M1VRf" +
                "7EMJvBtytrkHHDDG+DeNdmu1oNftVLdQts2pMjaLuisBu0Yug9i22gE3BoKSQ2UE" +
                "OFUyE09s/yvumesnbMWUeLBndtfilwMWtwS7torRcxLkfyCSxCqp+gKLwu8ws/ux" +
                "HXPAbIh9GR+7SQWOMTmQ07IUCLgpaO0Sk138DRPgz7/vwRFp9vQWiN8RDhyZaZI0" +
                "JxkX/1+qaBgQqLcxN297kFGWCKf4w4p07fgMqBpsDM9ObnVq8NO7N/sWH2Mvg202" +
                "4zewpAhMvZuQpEgpM8r8l6nQ/gXKw0E9xLnEuyaoMeqx/Cq9TcxVYeoCQGBVyrjx" +
                "06mAt/ZNAgMBAAECggEAZ+my7i7CqTgDxcWhKEgjtDEZaZlelCPdrfR4X+2QxD9f" +
                "6Lal1rFq2aaU7R1FtqaEVTjEzkJpA1w4/A07vw8Z5HZcBAStDcUb7JRA3RTkpTiF" +
                "DgdbvpGzhEBvdMezK8xj6r5ZnXh32zqUQDylTyMHJIExIfkztmeN6Lc6VIDv3h/9" +
                "csYm7Lf2oReg8bfZXyYiGxbW/MPzOHjHEm7qOVBfy+uX8lC2t5OxgtIIA1YJuwfv" +
                "iQruBokR0KTCfeBMiKQxv2sXka0X41UEW+BMLoIMhY1x28NI+dXasF6nhF11PmZv" +
                "zl1xJiPsEv/742CfF5rqRGQnz82e5QvQaDWAbY8PLQKBgQDnlP1R1M0vtZjllR9z" +
                "rKbaJIRAkVX2W6XJ7JzL9cCEktsgdNkJ7rFMAhvIC3gGKp2lchpet7+a+nYN565b" +
                "B89Du2z2AuGzh5eqieQwRlpDNtgljVAhTSMg+u2NERD3/r0KBJNpj+XQtOqDCTLn" +
                "iGOemUDoIS547nhYFUk2N2nOGwKBgQDYftzZseRoeLSD5iw+G1CjDAGk8Y8VhNwl" +
                "jClvJuxisO2rI84jTPISnJFxHICPa/hsJ/MqtrRqjWfhYn1Ee9jyqSDcean/5iNg" +
                "Ir9CM9RaQgv0O8l/RtpG4ebz9sIzzuXX5NYsxqcSNnqpBEzEXzWnuLW4C6+BVNpD" +
                "cVA/r3bztwKBgDU9qJuWm/G7A0LM7OUwLU/7kzgw8Z1o5o2sy0OM5UzVYnFsy7Uf" +
                "3j0GQxLTqC1qwMGXoP5jqjZcHKYHvKsK+Zitz1gjw3X9112KxB5hHkFTq4++lEyD" +
                "qm7boZ+L6+XH9/gEo0tinMhFv6COjdhzH8wHmceEUWXWFW7Svfkr9jQ/AoGAN46G" +
                "uk7OYIjpOhftLprv3IeOPHfI0irgG7qWP3Hro3/n3c04bxKl5GTNZgt8alG41HiE" +
                "DGgKcHWNPOd7YgGuMMd6KVoHaCX02gzQq/qE+gyM6MYW9VSHWz1JeT3PpF/p17df" +
                "xunLof7dB1aVJKeSK1JZP7uv9C6EPF5ty4oJqo0CgYEAvMh0y1J7G66mIVArn2+I" +
                "onyiwFwnWCzswNLEkhvVOfW8mgHcPPzb5UcVaLPtN5GWxL42dKJ0NEVfPF3Z8nl4" +
                "88+jWyKaqIMHh4yRHR39EwMIb29reqjT+yEBldGERbpuH0kf9Z0TkirqoZngoT/j" +
                "yQccmEQYR5w1Xv0eLbKs0GY=");
        pair.setPublicKey("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAw9h42rG9jNVUX+xDCbwbcra5Bxwwxvg3jXZrtaDX7VS3ULbNqTI2i7orAbtGLoPYttoBNwaCkkNlBDhVMhNPbP8r7pnrJ2zFlHiwZ3bX4pcDFrcEu7aK0XMS5H8gksQqqfoCi8LvMLP7sR1zwGyIfRkfu0kFjjE5kNOyFAi4KWjtEpNd/A0T4M+/78ERafb0FojfEQ4cmWmSNCcZF/9fqmgYEKi3MTdve5BRlgin+MOKdO34DKgabAzPTm51avDTuzf7Fh9jL4NtNuM3sKQITL2bkKRIKTPK/Jep0P4FysNBPcS5xLsmqDHqsfwqvU3MVWHqAkBgVcq48dOpgLf2TQIDAQAB");
        pair.setSecretKey("nsl7BTkDKyJdh6O055yPDAYAUJgFvi1Bvm904fSJg5I=");



//        String aeskey =  test.rsaUtil.getSecretAESKeyAsString();
//        String encryptedString = test.rsaUtil.encryptAESKey(aeskey, pair.getPublicKey());
//        System.out.println(encryptedString + "encryptedstring");
//        String decrypt = test.rsaUtil.decryptAESKey(encryptedString, pair.getPrivateKey());
//        System.out.println(decrypt + "decrypt");




//        String json =
//                {\n" +
//                        "    \"subServiceName\": \"Bills Payments\",\n" +
//                        "    \"errorCode\": \"E04\"\n" +
//                        "};


//        JSONObject object = new JSONObject(json);
//        System.out.println(object + "object");
//       MappedErrorRequest mapper = new ObjectMapper().convertValue(json, MappedErrorRequest.class);

//    MappedErrorRequest request = new MappedErrorRequest();
//    request.setSubServiceName("Bills Payments");
//    request.setErrorCode("E04");
//        ObjectMapper mapper = new ObjectMapper();
//        Map<String,Object> map = (Map<String, Object>) mapper.readValue(json, MappedErrorRequest.class);


//        System.out.println(test.encryptResponse(accountEnquiry, pair) + "request");

//        String aeskey = "58615e0ba49926bdb6e3dfa4148721a6439d45aec4835259a0f04ce22dcd89f6314799c4de2b943ccf38f5dd7d175183e1cd3528490de1bef3e1791272bbe141";

//        String request = "V/h7SLAZpZqGkUxohx+EPZFQOesLoeUs3OWTLAj4FyAbwGaCuDUtTg2ITBpRuzyLHng4k6SuN5YKWuqWX9moQP1Bosu9kMPxfiYSmMRnX4FEZZ/IYTFIN0LVT/zfi82BGXTCdTGU8OhW1LPxdl26G07eARkIGRe6IKD4G4BCfLsAtLVCMk+zRAusyyx2wlmNeXp3/gUEyJ4cGCcZ1NHEhB6jGEv2Vf3XZUymD3tEH+vNDid181CT9KgOTXQSDi93ir0FbS5VS69LnIkNT2xCMO1lAqRRYZj7wo/oXCxgRgt/R+voXAcNJRufa4S3zVNvbs03h/0YdzBsHDl10py69bj3mNwWu3szRYUVaeBg13cNd7O342JU/DHh0h7QxBKuEueINYrsGFa3zSxJ2a/kgtBallcpiaDKv6i9aYVSjw+T/Vq6RPFnHVpsCnrfyIrg6kLj39VdXIHEJhcfgQJ7jdix98odGOUoT8vA89TBaeOCW8+zyNnxYEC013bPQzzKWATYPHZWzqjY8nASjDvIFNnRPwqzZFiwgmgf7cbNJmMmhyP74GByV8c+tUVwbP6RDblt5LmIePU1h+7v6z62Se4fYzPBudijujqD2PWARKFbDEUogMld+C5QVyPlsW0YYtWi1vlKsnOpGoMubSihLs8ynEkFmmDumeMDraqxWqn/LFjInmp/IQrLlPgFlAfn9mL+rdzbUNeOyt4tMgweUPRaZ9R4/4XggEZQMrQItQb0I4IBsoSW1CAo11MHTxRMhyDmvEbQbuArRxqW+ZGxIFluWfCJ9pFjzIhppZCW1c5+T2S4Cgl38IUzNQKe3Xm+oHI1QNTtBe/cVUkXravjzqP1A2m3J/7zeMTX1Ew=";
//        String secret = "MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDUmtoPulYAz3ruMXVItEvMh/pFHoJ2IPSdNZ8r0QBV9R2SInAiWqQT1Ls2xMsdi2HxL27YHaPu92jHf1fBLybMRSESCI0BucapFu/LlAec2tq3mItm2sFPAeclM77Q0xb3qnBO6WjtrYYyMZj7ul3t/x+kgZfD6OGzmEII2/TgpdL3SML1gbv9zzZPRf9QvXCDBfgHUrjVpjgzQ3TK6OxhEQgEjtVcEE1XuMOO+u0bQNOYrfgL2N0gR34v48frFSyZIoaNXk244Y/g/z4zX2giBVxU9E+YLtEuHJ+dCXV84YI+hCbOgSrRJ12/IWo2g/L1Lv2twDNyERXDydnEydDXAgMBAAECggEBAJ96/YJ4y5TWDqfoz8rW/FkT02fP6BtGw1e+ZoUOxYq1URZS4PzUM2pw1HLUEm06JiEu7J4PNe7HBoFIBAL1JMxsOs91PJkDy4MBoqV6iG9gOFsqgb+I7yMeEU71zviEsCDeaFq5ei14UkcKr3B9L4KdMxZK5BZgdhYra7WLkdiFDKZK/TnZvYMC3rMATcuuFbKF7i8QTWwWVKUk0BhCdJ8bHdZphtePL3Sy3S34DUFWn3tqRJzPeCxM+/FzWzyPGP8wTuh+mZhmTLAcnoOmo7xfDqndshFATfwUGHwAPllrwNBuIa5vMs/900CCefmuBWS5nrbtl+3KJuEc+oSxUkkCgYEA+mjcVWoA0KOHL3e9yNFSS4zYt8QQnaTBsZxRly4iE/W/xUix2mJSYexHfeRrj4rkZSL8dwo4zFCjrwUvlIp8ZpoQ8+C7Xhl2JRifkhzCfRI5xnOUqwYWGv9vmT1xeMVQK3UEeGF35+TSabstKqHQzm4tHX63nCvYK6qxAbbls3sCgYEA2VnuCXG+K/+VGkggQ6QoP5geKAcRnND2r227L9t91qqmpCPs8zioRGoXk13Op6Gzx/AdbpO3xmNvVqJw2vOFcNQDLzg6Yby82YP3zXSwTaMo9B+5kxxRvIczb1KLoNRShWsRg3D2kiMINJ8lVfdxL4V/Qw2rE9k0lWi2onuB21UCgYB2O8zNB6Aa4xuRbhhOGYeh5H7vRcQSqxhJpGVh3IZ6pi5GGGzhkEsVsVqQNqoSvKHanwpVqp3m2qxulyRAHqNeTqByhknxxFPkcBvoAWxTYael7CzmE5mcZb9ru3c4bvHLkxTpQhw7ge9XvOk2l/Dfh2YF5y8KrikDdTTeel06wQKBgQCUZ78MW8mJqT1ZcJ33EYMZyPfuRMVV2ahWKAqDps7JYEPgF1eWyry52ea3DHoqI1Lp1Z2C/wGgSAyhjtCX72bRnzF9cFsMmR4pNt5gPqiSmjFAXtkLBW8Z1wbbn/UOn2qpBm4y3VK+fYUAyRlpFUcHN5OpaQ/CwbARxCnPz5ze8QKBgQCmUPWubnmqz6x99XSlXR4JQx4FMlrjJIfjKwj9demLqO0oMzBS2Prz5JHZ9EKbN62ravK58KHSN6hfnU2dc8jGF5zo9EKb3qv9+U2cQdKA9W7HyrKdSG5gxllkGhJVADAZ+4lUqEtWAFV413vTkG82T1KHNBBlCMYckOdnnJ2eUA==";
//        pair.setPrivateKey(secret);
//        System.out.println(test.decryptRequest(request, pair, aeskey, ServiceInformation.class));
//
        String response = "EA2fugkrsV8FJ5ZLh0EoIoHbwY31rYk9Lo+zHCZWeNMdrUC4r1+Qa8j7JcPKc7/XxYZZ8siNcTKxQj0o09s4kXCEJTgEdJFYsuoeQG7nurCV423dO8bzfOALCnuN4lx4Vvb6rOd2OgHfIIKzB1N5YquhGWiRGSKu1o1Y08g8VkVRaD1OpckBtzVa9GRCF+68lIZFu1cn6Ph54hzYFyP7o046Xb2WTGN+egyIqpTKXi5PHW6bPkL9bKiW/hB0exYagt2gAfOMlmGJxIGTOcR/RC+HtzZ2vdeunntIenm7keb6MWKNuYLXR4siOKWY52Xbvqy6TwTJeCAT2uOPFH3q5L9d2DVEUEOVXIcicN9d6xE5lpzM+CH1JHqU48MfJg4YeucJy2qpvs7WvT2EdmBFX95Jiiy7YM4yz2TVSf6to+nrlx2zcrgOJ5VMOmPn7iqaugmAFBAA9i7KvfiSiVJQuDJF7uoG6D7W+vl1vy2yrw15IIFRMKQ4p6CFlZfDjXiSdRlpSZxK1GvYt73PUfO4ZrJasGepsJKKtrJp1xHvN/uyIr9g6UUdoDUOC3tnenQO3b6TvH8n3V9WBPQT9evT62UUDbdIn12Su6ZOAHNugAMNXKRXv2zJI2AQfZRBCBfPz25daHJ2gVC0TZoH/rG5VNliJC3t9ZyKVxH6UcioVQCUctdc79miJbDllX5omAfdhPDrT5oP4vhV5XDbnsXOvfS0aLn17L+5MLYJ8A7GLHSUsOyMRa2b9gDfa4bZczQCrR7P0l9yxnOjg/MfF9w6yM8/nB7NNxbMgWUrIcORF0XbaBn46qi2LZevPkBsVoCaFhLnSNNzx/XBie56B9I9ZbGwNxyR/OZgJxZpcmBQ+McG9zqcJTn3I6BanOGMpHOwxTPG8F3ZpkO7mAl4EfqocfVXXOasNmgEg/qRWCaOitlWL376yyd0kf0QMz4D8ltdmWj7NVho7sk8vE/BuajE8vBhnjFROb0wWZxBiWXOOWUQjR3dyiVDdE9zJKwznt8wur302/fM9Xz9bQcCVnRN8UFi/bmiWptc8AbRt7cBwdpDbaYU24+GH0uVYDwu82cAkJgsqwVjBI1siil7jYkuSRSfB8NFEqvrZnbLHEf/FaV5dkE4s4P17gQ8mODqT0V0LaPIy7nnlO+N8Jo98IJu5p7NCUtKbdVOfEtUBLmZDnJVTFRT+//5bjo2uR7Qn1SQRjizV8gVNfqUNLtldGgsd2OnsAU21V+fJY5hP6t27HBs/CRDeBwi6Z6nqAopT0YS3U1EcIR9jw3O98smwE/Ja0uYcOGYL7GSXbhe1eZcEu5WQ56j/VP7TYEhWjaGq3lDj13zkxxr6O9SqqF5xmQVRUgqV5wZCuNiOla6b/9Y7yiEN8KPWKrTPZrFvEtvdNdni9/Z7eWk00tSP22FIpV11L7gR+jLcU5QS/lep1TLkGAj3vUipf5UFPYuVsit+nowlo8+Z/qLXljl4m0t4y2rGyIuqHLEk3SdKWj4yHEC+SYfbQYvyAcka77QVBmahidvqkJcsIwcCkfjvjNTcqsYbjuTV2f49kM0RYPneSRBtag+CezAMjSRX4lDjXYakojADhE8";
        String secretKey = "NWabHzd9KX4mXyJmWptEzk5U1cz2Bz9wuRrP2QLjO4No9Ak0UoeIcjJxy9RAEG7NmuZYDOEgpY0vIgHFRY/ZR67bUiS/lgLzKGpo5GE4vBTUJfj5LlSbxFBqVYiO3bI4Om1m6ECcZhqcNN2ED+cVjBOcECs/rmBgTn2DTMCZbgz69FX3GCDcj1YIB5QyTFCYAcQTpzCCmlRK9wEi9iV8621siz0OWq0ji2d0NnFQe7DFfNGjcnOlfMEsLcnN6qePwGmboSPJOAw2w5N6yqS9U+yayN2kf0Dw05Q04yWlkzkqHGynF/Ypx9wr2DVWqmprxi++s2inSKKfYuHJP+7J0g==";
        System.out.println(test.decryptRequest(response, pair, secretKey, ServiceInformation.class) + "response");
    }



}
