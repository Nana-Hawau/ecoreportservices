package com.ecobank.ecoreportservices.utils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

public class ObjectConverter {
    public static String marshalToXML(Object obj) throws Exception {

        ByteArrayOutputStream xmlStream = new ByteArrayOutputStream();

        JAXBContext jaxbContext = JAXBContext.newInstance(obj.getClass());
        Marshaller marshaller = jaxbContext.createMarshaller();
        //marshaller.setProperty("com.sun.xml.bind.xmlHeaders", "<?xml version=\"1.0\"?>");
        //marshaller.setProperty("com.sun.xml.bind.xmlDeclaration", Boolean.TRUE);
        //marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
        marshaller.marshal(obj, xmlStream);

        return new String(xmlStream.toByteArray());
    }

    public static Object marshalFromXML(String xmlValue, Class className) throws Exception {

        JAXBContext jaxbContext = JAXBContext.newInstance(className);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        XMLInputFactory xmlFactory = XMLInputFactory.newInstance();
        xmlFactory.setProperty(XMLInputFactory.SUPPORT_DTD, false);
        XMLStreamReader xmlReader = xmlFactory.createXMLStreamReader(new ByteArrayInputStream(xmlValue.getBytes("UTF-8")));
        JAXBElement<?> requestElement = unmarshaller.unmarshal(xmlReader, className);
        return requestElement.getValue();
    }


    public static String marshalToJSON(Object obj) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(obj);
    }

    @SuppressWarnings("deprecation")
    public static String marshalToJSONEscaped(Object obj) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(JsonGenerator.Feature.QUOTE_FIELD_NAMES, false);
        mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        return mapper.writeValueAsString(obj);
    }

    public static Object marshalFromJSON(String jsonValue, Class className) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(jsonValue, className);
    }
}
