package com.ecobank.ecoreportservices.utils;

import com.ecobank.ecoreportservices.constants.ResponseEnum;
import com.ecobank.ecoreportservices.models.AesKeyPair;
import com.ecobank.ecoreportservices.models.ResponseAuth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class ResponseUtils {

	@Autowired
	private SecurityUtils securityUtils;

	private ResponseEntity<ResponseAuth> getSuccess(String code, String message, Object data) {
		ResponseAuth response = new ResponseAuth();
		response.setCode(code);
		response.setMessage(message);
		response.setData(data);

		return ResponseEntity.ok(response);
	}

	private ResponseEntity<String> getSuccess(String code, String message, Object data, AesKeyPair keys)
			throws Exception {
		ResponseAuth response = new ResponseAuth();
		response.setCode(code);
		response.setMessage(message);
		response.setData(data);

		// encrypt response
		String[] encryptedResponse = securityUtils.encryptResponse(response, keys).split(",");
		HttpHeaders responseHeaders = new HttpHeaders();

		// add header
		responseHeaders.set("Content-Type", "text/plain");
		responseHeaders.set("x-secret-key", encryptedResponse[1]);

		return ResponseEntity.ok().headers(responseHeaders).body(encryptedResponse[0]);
	}

	private ResponseEntity<String> getSuccess(String code, String message, Object data, String key) throws Exception {
		ResponseAuth response = new ResponseAuth();
		response.setCode(code);
		response.setMessage(message);
		response.setData( data);

		// encrypt response
		String encryptedResponse = securityUtils.encryptResponse(response, key);
		HttpHeaders responseHeaders = new HttpHeaders();

		// add header
		responseHeaders.set("Content-Type", "text/plain");

		return ResponseEntity.ok().headers(responseHeaders).body(encryptedResponse);
	}

	public ResponseEntity<ResponseAuth> getSuccess(String code, String message) {
		ResponseAuth response = new ResponseAuth();
		response.setCode(code);
		response.setMessage(message);

		return ResponseEntity.ok(response);
	}

	public ResponseEntity<String> getSuccess(String code, String message, AesKeyPair keys) throws Exception {
		ResponseAuth response = new ResponseAuth();
		response.setCode(code);
		response.setMessage(message);

		// encrypt response
		String[] encryptedResponse = securityUtils.encryptResponse(response, keys).split(",");
		HttpHeaders responseHeaders = new HttpHeaders();

		// add header
		responseHeaders.set("Content-Type", "text/plain");
		responseHeaders.set("x-secret-key", encryptedResponse[1]);

		return ResponseEntity.ok().headers(responseHeaders).body(encryptedResponse[0]);
	}

	public ResponseEntity<ResponseAuth> getSuccess(ResponseEnum responseEnum) {
		ResponseAuth response = new ResponseAuth();
		response.setCode(responseEnum.getCode());
		response.setMessage(responseEnum.getMessage());

		return ResponseEntity.ok(response);
	}

	public ResponseEntity<String> getSuccess(ResponseEnum responseEnum, AesKeyPair keys) throws Exception {
		ResponseAuth response = new ResponseAuth();
		response.setCode(responseEnum.getCode());
		response.setMessage(responseEnum.getMessage());

		// encrypt response
		String[] encryptedResponse = securityUtils.encryptResponse(response, keys).split(",");
		HttpHeaders responseHeaders = new HttpHeaders();

		// add header
		responseHeaders.set("Content-Type", "text/plain");
		responseHeaders.set("x-secret-key", encryptedResponse[1]);

		return ResponseEntity.ok().headers(responseHeaders).body(encryptedResponse[0]);
	}

	public ResponseEntity<String> getSuccess(ResponseEnum responseEnum, String key) throws Exception {
		ResponseAuth response = new ResponseAuth();
		response.setCode(responseEnum.getCode());
		response.setMessage(responseEnum.getMessage());

		// encrypt response
		String encryptedResponse = securityUtils.encryptResponse(response, key);
		HttpHeaders responseHeaders = new HttpHeaders();

		// add header
		responseHeaders.set("Content-Type", "text/plain");

		return ResponseEntity.ok().headers(responseHeaders).body(encryptedResponse);
	}

	public ResponseEntity<ResponseAuth> getSuccess(ResponseEnum responseEnum, Object data) {
		ResponseAuth response = new ResponseAuth();
		response.setCode(responseEnum.getCode());
		response.setMessage(responseEnum.getMessage());
		response.setData(data);
		return ResponseEntity.ok(response);
	}

	public ResponseEntity<String> getSuccess(ResponseEnum responseEnum, Object data, AesKeyPair keys) throws Exception {
		ResponseAuth response = new ResponseAuth();
		response.setCode(responseEnum.getCode());
		response.setMessage(responseEnum.getMessage());
		response.setData(data);

		// encrypt response
		String[] encryptedResponse = securityUtils.encryptResponse(response, keys).split(",");
		HttpHeaders responseHeaders = new HttpHeaders();

		// add header
		responseHeaders.set("Content-Type", "text/plain");
		responseHeaders.set("x-secret-key", encryptedResponse[1]);

		return ResponseEntity.ok().headers(responseHeaders).body(encryptedResponse[0]);
	}
	
	public ResponseEntity<String> getSuccess(ResponseEnum responseEnum, Object data, String key) throws Exception {
		ResponseAuth response = new ResponseAuth();
		response.setCode(responseEnum.getCode());
		response.setMessage(responseEnum.getMessage());
		response.setData( data);

		// encrypt response
		String encryptedResponse = securityUtils.encryptResponse(response, key);
		HttpHeaders responseHeaders = new HttpHeaders();

		// add header
		responseHeaders.set("Content-Type", "text/plain");

		return ResponseEntity.ok().headers(responseHeaders).body(encryptedResponse);
	}

	public ResponseEntity<ResponseAuth> getError(String code, String message) {
		ResponseAuth response = new ResponseAuth();
		response.setCode(code);
		response.setMessage(message);
		// response.setData(data);

		return ResponseEntity.ok(response);
	}

	public ResponseEntity<String> getError(String code, String message, AesKeyPair keys) throws Exception {
		ResponseAuth response = new ResponseAuth();
		response.setCode(code);
		response.setMessage(message);
		// response.setData(data);

		// encrypt response
		String[] encryptedResponse = securityUtils.encryptResponse(response, keys).split(",");
		HttpHeaders responseHeaders = new HttpHeaders();

		// add header
		responseHeaders.set("Content-Type", "text/plain");
		responseHeaders.set("x-secret-key", encryptedResponse[1]);

		return ResponseEntity.ok().headers(responseHeaders).body(encryptedResponse[0]);
	}
	
	public ResponseEntity<String> getError(String code, String message, String key) throws Exception {
		ResponseAuth response = new ResponseAuth();
		response.setCode(code);
		response.setMessage(message);
		// response.setData(data);

		// encrypt response
		String encryptedResponse = securityUtils.encryptResponse(response, key);
		HttpHeaders responseHeaders = new HttpHeaders();

		// add header
		responseHeaders.set("Content-Type", "text/plain");
		
		return ResponseEntity.ok().headers(responseHeaders).body(encryptedResponse);
	}

	public ResponseEntity<ResponseAuth> getError(ResponseEnum responseEnum, Object data) {
		ResponseAuth response = new ResponseAuth();
		response.setCode(responseEnum.getCode());
		response.setMessage(responseEnum.getMessage());
		response.setData(data);

		return ResponseEntity.ok(response);
	}

	public ResponseEntity<String> getError(ResponseEnum responseEnum, Object data, AesKeyPair keys) throws Exception {
		ResponseAuth response = new ResponseAuth();
		response.setCode(responseEnum.getCode());
		response.setMessage(responseEnum.getMessage());
		response.setData((List<Object>) data);

		// encrypt response
		String[] encryptedResponse = securityUtils.encryptResponse(response, keys).split(",");
		HttpHeaders responseHeaders = new HttpHeaders();

		// add header
		responseHeaders.set("Content-Type", "text/plain");
		responseHeaders.set("x-secret-key", encryptedResponse[1]);

		return ResponseEntity.ok().headers(responseHeaders).body(encryptedResponse[0]);
	}
	
	public ResponseEntity<String> getError(ResponseEnum responseEnum, Object data, String key) throws Exception {
		ResponseAuth response = new ResponseAuth();
		response.setCode(responseEnum.getCode());
		response.setMessage(responseEnum.getMessage());
		response.setData(data);

		// encrypt response
		String encryptedResponse = securityUtils.encryptResponse(response, key);
		HttpHeaders responseHeaders = new HttpHeaders();

		// add header
		responseHeaders.set("Content-Type", "text/plain");

		return ResponseEntity.ok().headers(responseHeaders).body(encryptedResponse);
	}


	public ResponseEntity<ResponseAuth> getError(ResponseEnum responseEnum) {
		ResponseAuth response = new ResponseAuth();
		response.setCode(responseEnum.getCode());
		response.setMessage(responseEnum.getMessage());
		// response.setData(data);

		return ResponseEntity.ok(response);
	}

	public ResponseEntity<String> getError(ResponseEnum responseEnum, AesKeyPair keys) throws Exception {
		ResponseAuth response = new ResponseAuth();
		response.setCode(responseEnum.getCode());
		response.setMessage(responseEnum.getMessage());
		// response.setData(data);

		// encrypt response
		String[] encryptedResponse = securityUtils.encryptResponse(response, keys).split(",");
		HttpHeaders responseHeaders = new HttpHeaders();

		// add header
		responseHeaders.set("Content-Type", "text/plain");
		responseHeaders.set("x-secret-key", encryptedResponse[1]);

		return ResponseEntity.ok().headers(responseHeaders).body(encryptedResponse[0]);
	}
	
	public ResponseEntity<String> getError(ResponseEnum responseEnum, String key) throws Exception {
		ResponseAuth response = new ResponseAuth();
		response.setCode(responseEnum.getCode());
		response.setMessage(responseEnum.getMessage());
		// response.setData(data);

		// encrypt response
		String encryptedResponse = securityUtils.encryptResponse(response, key);
		HttpHeaders responseHeaders = new HttpHeaders();

		// add header
		responseHeaders.set("Content-Type", "text/plain");

		return ResponseEntity.ok().headers(responseHeaders).body(encryptedResponse);
	}


}
