package com.ecobank.ecoreportservices.utils.encryptionutils;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

@Service
@Primary
public class RsaAesOfbStandard implements RsaAesStandard {

	private static final String RSA_ENCRYPT_ALGO = "RSA/ECB/OAEPWithSHA-256AndMGF1Padding"; // RSA Algorithm/Mode/Padding
	private static final String AES_ENCRYPT_ALGO = "AES/OFB/NoPadding"; // AES Algorithm/Mode/Padding
	private static final Charset UTF_8 = StandardCharsets.UTF_8;
	private static final int IV_LENGTH_BYTE = 16; // Length of initialization vector

	@Override
	public String getSecretAESKeyAsString() throws Exception {
		KeyGenerator generator = KeyGenerator.getInstance("AES");
		generator.init(256); // The AES key size in number of bits
		SecretKey secKey = generator.generateKey();
		String encodedKey = Base64.getEncoder().encodeToString(secKey.getEncoded());
		return encodedKey;
	}

	@Override
	public String encryptAESKey(String plainAESKey, String publicKey) throws Exception {
		Cipher cipher = Cipher.getInstance(RSA_ENCRYPT_ALGO,"SunJCE");
		cipher.init(Cipher.ENCRYPT_MODE, getPublicKey(publicKey));
		return Base64.getEncoder().encodeToString(cipher.doFinal(Base64.getDecoder().decode(plainAESKey)));
	}

	@Override
	public String decryptAESKey(String data, String base64PrivateKey) throws Exception {
		Cipher cipher = Cipher.getInstance(RSA_ENCRYPT_ALGO,"SunJCE");
		cipher.init(Cipher.DECRYPT_MODE, getPrivateKey(base64PrivateKey));
		return Base64.getEncoder().encodeToString(cipher.doFinal(Base64.getDecoder().decode(data)));
	}

	@Override
	public String encryptTextUsingAES(String plainText, String aesKeyString) throws Exception {
		byte[] decodedKey = Base64.getDecoder().decode(aesKeyString);
		byte[] iv = getRandomNonce(IV_LENGTH_BYTE);
		
		SecretKey originalKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");
		Cipher aesCipher = Cipher.getInstance(AES_ENCRYPT_ALGO,"SunJCE");
		aesCipher.init(Cipher.ENCRYPT_MODE, originalKey, new IvParameterSpec(iv));
		byte[] byteCipherText = aesCipher.doFinal(plainText.getBytes(UTF_8));
		byte[] cipherTextWithIv = ByteBuffer.allocate(iv.length + byteCipherText.length).put(iv).put(byteCipherText)
				.array();

		return Base64.getEncoder().encodeToString(cipherTextWithIv);
	}

	@Override
	public String decryptTextUsingAES(String encryptedText, String aesKeyString) throws Exception {
		byte[] decodedKey = Base64.getDecoder().decode(aesKeyString);
		SecretKey originalKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");

		byte[] decode = Base64.getDecoder().decode(encryptedText.getBytes(UTF_8));
		ByteBuffer bb = ByteBuffer.wrap(decode);
		byte[] iv = new byte[IV_LENGTH_BYTE];
		bb.get(iv);
		byte[] cipherText = new byte[bb.remaining()];
		bb.get(cipherText);
		
		Cipher cipher = Cipher.getInstance(AES_ENCRYPT_ALGO,"SunJCE");
		cipher.init(Cipher.DECRYPT_MODE, originalKey, new IvParameterSpec(iv));
		byte[] plainText = cipher.doFinal(cipherText);

		return new String(plainText, UTF_8);
	}

	protected PublicKey getPublicKey(String base64PublicKey) {
		PublicKey publicKey = null;
		try {
			X509EncodedKeySpec keySpec = new X509EncodedKeySpec(Base64.getDecoder().decode(base64PublicKey.getBytes()));
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			publicKey = keyFactory.generatePublic(keySpec);
			return publicKey;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}
		return publicKey;
	}

	protected PrivateKey getPrivateKey(String base64PrivateKey) {
		PrivateKey privateKey = null;
		PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(base64PrivateKey.getBytes()));
		KeyFactory keyFactory = null;
		try {
			keyFactory = KeyFactory.getInstance("RSA");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		try {
			privateKey = keyFactory.generatePrivate(keySpec);
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}
		return privateKey;
	}
	
	protected byte[] getRandomNonce(int len) {
		byte[] nonce = new byte[len];
		new SecureRandom().nextBytes(nonce);
		return nonce;
	}	

}
