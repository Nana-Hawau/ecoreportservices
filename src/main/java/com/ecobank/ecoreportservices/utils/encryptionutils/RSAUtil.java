package com.ecobank.ecoreportservices.utils.encryptionutils;

import org.springframework.stereotype.Component;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

@Component
public class RSAUtil {

	static private final String ENCODING = "UTF-8";
	private static final String ENCRYPT_ALGO = "AES/GCM/NoPadding"; // AES Algorithm/Mode/Padding
	private static final int TAG_LENGTH_BIT = 128; // Length of authentication tag
	private static final int IV_LENGTH_BYTE = 12; // Length of initialization vector
	private static final Charset UTF_8 = StandardCharsets.UTF_8;

	// Create a new AES key. Uses 256 bit
	public String getSecretAESKeyAsString() throws Exception {
		KeyGenerator generator = KeyGenerator.getInstance("AES");
		generator.init(256); // The AES key size in number of bits
		SecretKey secKey = generator.generateKey();
		String encodedKey = Base64.getEncoder().encodeToString(secKey.getEncoded());
		return encodedKey;
	}

	// Encrypt AES Key using RSA private key
	public String encryptAESKey(String plainAESKey, String publicKey) throws Exception {
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.ENCRYPT_MODE, getPublicKey(publicKey));
		return Base64.getEncoder().encodeToString(cipher.doFinal(Base64.getDecoder().decode(plainAESKey)));
	}

	public String decryptAESKey(String data, String base64PrivateKey) throws Exception {
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.DECRYPT_MODE, getPrivateKey(base64PrivateKey));
		return Base64.getEncoder().encodeToString(cipher.doFinal(Base64.getDecoder().decode(data)));
	}

	public String encryptTextUsingAES(String plainText, String aesKeyString) throws Exception {
		byte[] decodedKey = Base64.getDecoder().decode(aesKeyString);
		byte[] iv = getRandomNonce(IV_LENGTH_BYTE);

		SecretKey originalKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");
		Cipher aesCipher = Cipher.getInstance(ENCRYPT_ALGO);
		aesCipher.init(Cipher.ENCRYPT_MODE, originalKey, new GCMParameterSpec(TAG_LENGTH_BIT, iv));

		byte[] byteCipherText = aesCipher.doFinal(plainText.getBytes(UTF_8));
		// We prefix the IV to the encrypted text, because we need the same IV for Decryption
		byte[] cipherTextWithIv = ByteBuffer.allocate(iv.length + byteCipherText.length).put(iv).put(byteCipherText)
				.array();

		return Base64.getEncoder().encodeToString(cipherTextWithIv);
	}


	public String decryptTextUsingAES(String encryptedText, String aesKeyString) throws Exception {
		byte[] decodedKey = Base64.getDecoder().decode(aesKeyString);
		SecretKey originalKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");

		byte[] decode = Base64.getDecoder().decode(encryptedText.getBytes(UTF_8));
		ByteBuffer bb = ByteBuffer.wrap(decode);
		byte[] iv = new byte[IV_LENGTH_BYTE];
		bb.get(iv);
		byte[] cipherText = new byte[bb.remaining()];
		bb.get(cipherText);

		Cipher cipher = Cipher.getInstance(ENCRYPT_ALGO);
		cipher.init(Cipher.DECRYPT_MODE, originalKey, new GCMParameterSpec(TAG_LENGTH_BIT, iv));
		byte[] plainText = cipher.doFinal(cipherText);

		return new String(plainText, UTF_8);
	}

	private static byte[] getKeyBytes(String key) throws UnsupportedEncodingException {
		byte[] keyBytes = new byte[16];
		try {
			byte[] parameterKeyBytes = key.getBytes(ENCODING);
			System.arraycopy(parameterKeyBytes, 0, keyBytes, 0, Math.min(parameterKeyBytes.length, keyBytes.length));
		} catch (UnsupportedEncodingException e) {
			throw e;
		}
		return keyBytes;
	}

	// Encrypt text using AES key
//	public String encryptTextUsingAES(String plainText, String aesKeyString) throws Exception {
//		byte[] decodedKey = Base64.getDecoder().decode(aesKeyString);
//		SecretKey originalKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");
//
//		// AES defaults to AES/ECB/PKCS5Padding in Java 7
//		Cipher aesCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
//		aesCipher.init(Cipher.ENCRYPT_MODE, originalKey);
//		byte[] byteCipherText = aesCipher.doFinal(plainText.getBytes());
//		return Base64.getEncoder().encodeToString(byteCipherText);
//	}
//
//	// Decrypt text using AES key
//	public String decryptTextUsingAES(String encryptedText, String aesKeyString) throws Exception {
//
//		byte[] decodedKey = Base64.getDecoder().decode(aesKeyString);
//		SecretKey originalKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");
//
//		// AES defaults to AES/ECB/PKCS5Padding in Java 7
//		Cipher aesCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
//		aesCipher.init(Cipher.DECRYPT_MODE, originalKey);
//		byte[] bytePlainText = aesCipher.doFinal(Base64.getDecoder().decode(encryptedText));
//		return new String(bytePlainText);
//	}

	private PublicKey getPublicKey(String base64PublicKey) {
		PublicKey publicKey = null;
		try {
			X509EncodedKeySpec keySpec = new X509EncodedKeySpec(Base64.getDecoder().decode(base64PublicKey.getBytes()));
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			publicKey = keyFactory.generatePublic(keySpec);
			return publicKey;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}
		return publicKey;
	}

	private PrivateKey getPrivateKey(String base64PrivateKey) {
		PrivateKey privateKey = null;
		PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(base64PrivateKey.getBytes()));
		KeyFactory keyFactory = null;
		try {
			keyFactory = KeyFactory.getInstance("RSA");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		try {
			privateKey = keyFactory.generatePrivate(keySpec);
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}
		return privateKey;
	}
	protected byte[] getRandomNonce(int len) {
		byte[] nonce = new byte[len];
		new SecureRandom().nextBytes(nonce);
		return nonce;
	}

	// Decrypt AES Key using RSA public key
	private static String decryptAESKey(byte[] encryptedAESKey, PrivateKey privateKey) throws Exception {
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.DECRYPT_MODE, privateKey);
		return Base64.getEncoder().encodeToString(cipher.doFinal(encryptedAESKey));
	}

	public String decryptData(String data, String base64PrivateKey) throws Exception {
		return decryptAESKey(Base64.getDecoder().decode(data.getBytes()), getPrivateKey(base64PrivateKey));
	}

	/**
	 * Encrypt data to third party
	 * 
	 * @param rawData   //@param plainAESKey
	 * @param publicKey
	 * @return String
	 * @throws Exception
	 */
	public String encrypt(String rawData, String publicKey) throws Exception {

		// generate Random AES Key
		String secretAESKeyString = getSecretAESKeyAsString();

		// Encrypt our data with AES key
		String encryptedText = encryptTextUsingAES(rawData, secretAESKeyString);

		// Encrypt AES Key with RSA Public Key
		String encryptedAESKeyString = encryptAESKey(secretAESKeyString, publicKey);

		return encryptedText + "," + encryptedAESKeyString;
	}

	/**
	 * Decrypt Encrypted Data.
	 * 
	 * @param encryptedData
	 * @param encryptedAESKeyString
	 * @param privateKey
	 * @return String the decrypted data
	 * @throws Exception
	 */
	public String decrypt(String encryptedData, String encryptedAESKeyString, String privateKey) throws Exception {
		// First decrypt the AES Key with RSA Private key
		String decryptedAESKeyString = decryptData(encryptedAESKeyString, privateKey);

		// Now decrypt data using the decrypted AES key!
		String decryptedData = decryptTextUsingAES(encryptedData, decryptedAESKeyString);
		return decryptedData;
	}

	public static void main(String[] args) {
		RSAUtil util = new RSAUtil();
		try {
			String secretAESKeyAsString = util.getSecretAESKeyAsString();
			System.out.println("Generated AES KEY >> "+ secretAESKeyAsString);
			String res = "{\n" +
					"    \"accountNumber\":\"0023123747\",\n" +
					"    \"transactionAmount\":10000000,\n" +
					"    \"currencyCode\":\"NGN\",\n" +
					"    \"transactionReference\":\"PXX1234567\",\n" +
					"    \"transactionType\":\"DEBIT\",\n" +
					"    \"requestTime\":\"\",\n" +
					"    \"isoCountryCode\":\"NG\"\n" +
					"\n" +
					"}";
			String encryptedText = util.encryptTextUsingAES(res, secretAESKeyAsString);
			String encryptedAESKeyString = util.encryptAESKey(secretAESKeyAsString, "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoGPj6W+xKtLbTbvY71Tu" +
					"azZ4NG7Zn2+X00IBjkzz1/mziRwb0frYCunuoguBlaeObLNUvWGr7fHSM7nfjJt9" +
					"TOU2pJ15Fuq12qzg7nPRlBTzl+salPGkGVVZGiN/N4QeNa4Vj+bPN+u2unzIAm6Q" +
					"AKOCuGvAqq082SpoPZXJBbarCi42oD60mVeP0RCoFY732aJn4qAHA6qUDSndDiUx" +
					"yH0Eb9rY+gFWjmiPHAAtlaHMgfu8nzrUMMDzvlF2HV9Ih2AC/M0sB9Z1zP2cfa1f" +
					"0OQRVOTo4WfF5XsuoESqs/vBSsMyRbQiVwPPMoGxV2suJSGzQzGZBlUi7EHBHr8C" +
					"fwIDAQAB");
			System.out.println("Payload >> " +res);
			System.out.println("Encrypted Payload >>> "+encryptedText);
			System.out.println("encryptes AES String >>> "+ encryptedAESKeyString);


			String decryptedKey = util.decryptAESKey(encryptedAESKeyString,"MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCgY+Ppb7Eq0ttN" +
					"u9jvVO5rNng0btmfb5fTQgGOTPPX+bOJHBvR+tgK6e6iC4GVp45ss1S9Yavt8dIz" +
					"ud+Mm31M5TaknXkW6rXarODuc9GUFPOX6xqU8aQZVVkaI383hB41rhWP5s8367a6" +
					"fMgCbpAAo4K4a8CqrTzZKmg9lckFtqsKLjagPrSZV4/REKgVjvfZomfioAcDqpQN" +
					"Kd0OJTHIfQRv2tj6AVaOaI8cAC2VocyB+7yfOtQwwPO+UXYdX0iHYAL8zSwH1nXM" +
					"/Zx9rV/Q5BFU5OjhZ8Xley6gRKqz+8FKwzJFtCJXA88ygbFXay4lIbNDMZkGVSLs" +
					"QcEevwJ/AgMBAAECggEASj3ywZtx5uCzosfRIvetZPNRsPOeyK5fGOr0zWnRdaef" +
					"81ePiSMT3flq/JdW5G8q/iYEk6UicIH/SvI/CFbxKSbwpEJASttaUl6zv6zc1i7c" +
					"kvXA2DECRLe9BtxnhsshMLfkQzgTs07oxmn8KyQChEkruTInfNmz0NJQbjFnW7z2" +
					"lnyNLaix2U20LDqS4ZD/tGH3fthGGbVFUtRce+ta3AJ9QZ8yk4UkbjWIPsQH/Gkb" +
					"vIAZ63UdRMcKGrNDH/9YhvRIwICEIcteOwu6veGUhKnwzlQyCn6gjApjaxGq+XMN" +
					"/MG1W1GQ9kdGLGSLCToXKJ0PiRGIgj+PKc+YoALIAQKBgQDVcWbn1kKwfl9mp0AO" +
					"S/LZmDQkq2gZjcqOLmofxXd/FdnRk//G8/cWB2Fdt0MZkOP7QMkBxBHY4wwv9Czy" +
					"P3Q4nb5hJGkh1kAJY2VZ196hfer5sI33YLqttPXYt5a/25Ra8iDiOyVrjeORDNaY" +
					"20qx5IDCz7xe0KgxITdpD31zOQKBgQDAXo7WiLg9VmHnUwHBhC0ewBD/baJIUBu1" +
					"3ohAYxOU7lbVY7eGQPqoVJTGjjizVvXIS8PSytSnpFpx88BZCaTqDbV7mArjBKE5" +
					"lyXZQAhgkRiBkFB1kHfNkV+rVMnQ09RvH8rvqUirYK9lIyXEYrMdceAyBQBruwIh" +
					"gvMZjFMLdwKBgQCTnatK2Z8jyzKVn0Qo3xPks2QUnEsRqUOWU7lnob0EMouDa3Tx" +
					"Ftci8BXBp7YLX26c3PBnfT/OnTBXlerj20i4wzfgnTrsgO2m0IxOt4IsdBZne1ij" +
					"5s0Ux9tjd40VwXn38A8YB0nY+beSIKvl6XveTidTFDK17F72I3/b9C+R2QKBgQCu" +
					"BhyjuUUqv/WLhc0F2mPS+J+n4u83Cbk3utt5m+eACGjnW/EPc8MN+o4oXUqv+pAY" +
					"JWo+TB06lvT3NzIfmhjlX4d5Cp92pZ6QccrckQ/DBB+uPfZ7lDi877EjVm3IC4mX" +
					"CaTVUEgcOTgSf0xQL5WnonfV6C5sRt3zoPn4d7g8mwKBgCDMMyEbbilTKMYmSZAt" +
					"IKkXx9GvDAgUDaj5DhRWk3yN15JtHYzqopZDnjv5hgPibztiPpeIv9SsWoJhqaZf" +
					"JZyEzhW+kpTT8AqMVMayqN1elx91yqlUPeYc/Il0WWjKGb139uZ6SLnWsY40TLyT" +
					"biJBSLwkEvbRpwHxw4jv8BtT");

			System.out.println("Decrypted AES key: "+ decryptedKey);
			String decryptedResponse = util.decryptTextUsingAES(encryptedText, decryptedKey);
			System.out.println("Decrypted request::: "+ decryptedResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
