package com.ecobank.ecoreportservices.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.util.Map;
import java.util.TreeMap;

public class AffiliateDataSources {
    private static final Logger logger = LoggerFactory.getLogger(AffiliateDataSources.class);

    private final Map<String, DataSource> dataSources = new TreeMap<String, DataSource>();

    public DataSource getDataSource(String affiliate) {
        logger.info("MY AFFILIATE" + affiliate);

        logger.info("AffiliateDataSources 2 Connection+++" + dataSources.get(affiliate));
        return dataSources.get(affiliate);
    }

    public void addDataSource(String affiliate, DataSource dataSource) {
        dataSources.put(affiliate, dataSource);
    }
}
