package com.ecobank.ecoreportservices.utils;


import com.ecobank.ecoreportservices.constants.ResponseEnum;
import com.ecobank.ecoreportservices.daos.AuthDaoImplemention;
import com.ecobank.ecoreportservices.exceptions.MissingHeaderException;
import com.ecobank.ecoreportservices.models.AesKeyPair;
import com.ecobank.ecoreportservices.utils.encryptionutils.RsaAesStandard;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.servlet.http.HttpServletRequest;


@Component
public class SecurityUtils {

	@Autowired
	AuthDaoImplemention authDaoImplemention;
	
	private static Logger logger = LoggerFactory.getLogger(SecurityUtils.class);
	private final static ObjectMapper objectMapper = new ObjectMapper();
	
	private final RsaAesStandard rsaUtil;

	public SecurityUtils(RsaAesStandard rsaUtil) {
		super();
		this.rsaUtil = rsaUtil;
	}
	

	public AesKeyPair extractKeys(final HttpServletRequest request) throws Exception {
		String clientId = request.getHeader("x-client-id");
		String clientSecret = request.getHeader("x-client-secret");
		String sourceCode = request.getHeader("x-source-code");
		logger.info("Header Keys {} :", clientId + clientSecret + sourceCode);
		AesKeyPair keys = doAuth(clientId, clientSecret, sourceCode);
		return keys;
	}

	public AesKeyPair doAuth(String clientId, String clientSecret, String sourceCode) throws Exception {
		if (clientId == null || clientSecret == null|| sourceCode == null) {
			throw new MissingHeaderException(ResponseEnum.INVALID_REQUEST);
		}
		return authDaoImplemention.authenticateClient(clientId, clientSecret, sourceCode);

	}
	
	public <T> String encryptResponse(T response, AesKeyPair key) throws Exception {
		String encryptedRes = null;
		try {
			String res = objectMapper.writeValueAsString(response);
			//encryptionofrequest
			String aeskey = rsaUtil.getSecretAESKeyAsString();
			String encryptedText = rsaUtil.encryptTextUsingAES(res, aeskey);
			String encryptedAESKeyString = rsaUtil.encryptAESKey(aeskey, key.getPublicKey());
			encryptedRes =  encryptedText + "," + encryptedAESKeyString;
			logger.info("Ecoreport Services response encryption successful");
		} catch (JsonProcessingException e) {
			logger.error(e.toString());
			throw e;
		} catch (Exception e) {
			logger.error(e.toString(),e);
			throw e;
		}

		return encryptedRes;
	}
	

	public <T> T decryptRequest(String request, AesKeyPair key, String encryptedAesKey, Class<T> elementClass) throws Exception {
		T response = null;
		String decryptedKey;
		String decryptedResponse;
		try {
			decryptedKey = rsaUtil.decryptAESKey(encryptedAesKey, key.getPrivateKey());
			logger.info("Decrypted AES key: {}", decryptedKey);
			decryptedResponse = rsaUtil.decryptTextUsingAES(request, decryptedKey);
			logger.info("Ecoreport Services request decryption successful");
			logger.info("Decrypted request::: {}", decryptedResponse);
			response = objectMapper.readValue(decryptedResponse, elementClass);
			logger.info("Request::: {}", response);
		} catch (JsonProcessingException e) {
			logger.error(e.toString());
			throw e;
		} catch (Exception e) {
			logger.error(e.toString(),e);
			throw e;
		}

		return response;
	}

	
	public <T> String encryptResponse(T response, String key) throws Exception {
		if (key == null) {
			throw new NullPointerException("Could not retrieve Client Encryption Key");
		}
		String encryptedRes = null;
		try {
			String res = objectMapper.writeValueAsString(response);
			encryptedRes = rsaUtil.encryptTextUsingAES(res, key);
			logger.info("Ecoreport Services response encryption successful");
		} catch (JsonProcessingException e) {
			logger.error(e.toString());
			throw e;
		} catch (Exception e) {
			logger.error(e.toString(),e);
			throw e;
		}

		return encryptedRes;
	}
	

	public <T> T decryptRequest(String request, String key, Class<T> elementClass) throws Exception {
		if (key == null) {
			throw new NullPointerException("Could not retrieve Client Decryption Key");
		}
		T response = null;
		String decryptedResponse;
		try {
			decryptedResponse = rsaUtil.decryptTextUsingAES(request, key);
			logger.info("Ecoreport Services request decryption successful");
			response = objectMapper.readValue(decryptedResponse, elementClass);
			logger.info("Request::: {}", response);
		} catch (JsonProcessingException e) {
			logger.error(e.toString());
			throw e;
		} catch (Exception e) {
			logger.error(e.toString(),e);
			throw e;
		}

		return response;
	}








}
