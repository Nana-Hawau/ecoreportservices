package com.ecobank.ecoreportservices.utils;

import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;

@Service
public class DateValidation {
    final static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d-MM-yy");
    final static long monthLimit = 3;

    public static Boolean isValidDate(String startDate, String endDate) {
        System.out.println(Period.between(LocalDate.parse(startDate, formatter), LocalDate.parse(endDate, formatter)));
        System.out.println(Period.between(LocalDate.parse(startDate, formatter), LocalDate.parse(endDate, formatter)).getYears());
        return Period.between(LocalDate.parse(startDate, formatter), LocalDate.parse(endDate, formatter)).getMonths()
                > monthLimit || Period.between(LocalDate.parse(startDate, formatter), LocalDate.parse(endDate, formatter)).getYears()
                >= 1;
    }

}
