package com.ecobank.ecoreportservices.utils;
import com.ecobank.ecoreportservices.constants.MethodConstants;
import com.ecobank.ecoreportservices.daos.cards.CardServicesDaoImpl;
import com.ecobank.ecoreportservices.models.CardsSearchDataRequest;
import com.ecobank.ecoreportservices.models.CardsSearchDataRespModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class HelperMethods {



    @Autowired
    CardServicesDaoImpl cardServicesDao;

    public String getMobileServicesMethodName(String subService) {
        String methodName;
        switch (subService) {
            case ("Bills Payment"):
            case ("Domestic Transfer"):
                methodName = MethodConstants.BILLS_PAYMENT_METHOD_NAME;
                break;
            case ("Inter Affiliate Transfer"):
                methodName = MethodConstants.INTER_AFF_METHOD_NAME;
                break;
            case ("Inter Bank"):
                methodName = MethodConstants.INTER_BANK_METHOD_NAME;
                break;
            case ("International Transfer"):
                methodName = MethodConstants.INTL_TRANSFER_METHOD_NAME;
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + subService);
        }

        return methodName;

    }



    public List<CardsSearchDataRespModel> getSearchDataCards (CardsSearchDataRequest cardsSearchDataRequest) {

        List<CardsSearchDataRespModel> response = new ArrayList<>();

        String cluster = cardsSearchDataRequest.getCluster();

        try {

            if(cluster.equalsIgnoreCase("ENG")){
                response = cardServicesDao.getSearchDataCards(cardsSearchDataRequest);
            }
//            else if(cluster.equalsIgnoreCase("AWA")){
//                response = cardServicesDao.getSearchDataCardsAWA(cardsSearchDataRequest);
//            }
//            else if(cluster.equalsIgnoreCase("FWA")){
//                response = cardServicesDao.getSearchDataCardsFWA(cardsSearchDataRequest);
//            }
//            else if(cluster.equalsIgnoreCase("CESA1")){
//                response = cardServicesDao.getSearchDataCardsCESA1(cardsSearchDataRequest);
//            }
//            else if(cluster.equalsIgnoreCase("CESA2")){
//                response = cardServicesDao.getSearchDataCardsCESA2(cardsSearchDataRequest);
//            }

        }catch (Exception e){
            e.printStackTrace();
        }

        return response;
    }



}
