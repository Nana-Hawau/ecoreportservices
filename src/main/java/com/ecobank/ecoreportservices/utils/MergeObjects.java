package com.ecobank.ecoreportservices.utils;

import org.springframework.beans.BeanUtils;

public class MergeObjects {

    public static <T> T mergeObjects(T a, T b, T c) throws InstantiationException, IllegalAccessException {
        // would require a noargs constructor for the class, maybe you have a different way to create the result.
        T result = (T) a.getClass().newInstance();
        BeanUtils.copyProperties(a, result);
        BeanUtils.copyProperties(b, result);
        BeanUtils.copyProperties(c, result);
        return result;
    }
}
