package com.ecobank.ecoreportservices.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SubServices {

    @JsonProperty("name")
    private String subServiceName;

    private String subServiceCode;

    public SubServices() {
    }

    public SubServices(String subServiceName, String subServiceCode) {
        this.subServiceName = subServiceName;
        this.subServiceCode = subServiceCode;
    }

    public String getSubServiceCode() {
        return subServiceCode;
    }

    public String getSubServiceName() {
        return subServiceName;
    }

    public void setSubServiceName(String subServiceName) {
        this.subServiceName = subServiceName;
    }

    public void setSubServiceCode(String subServiceCode) {
        this.subServiceCode = subServiceCode;
    }

    @Override
    public String toString() {
        return "SubServices{" +
                "subServiceName='" + subServiceName + '\'' +
                ", subServiceCode='" + subServiceCode + '\'' +
                '}';
    }
}
