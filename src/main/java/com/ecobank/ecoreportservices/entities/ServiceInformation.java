package com.ecobank.ecoreportservices.entities;

import java.util.List;

public class ServiceInformation {
    private String serviceName;
    private String serviceCode;
    private List<SubServices> subServices;

    public ServiceInformation() {
    }

    public ServiceInformation(String serviceName, String serviceCode, List<SubServices> subServices) {
        this.serviceName = serviceName;
        this.serviceCode = serviceCode;
        this.subServices = subServices;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public List<SubServices> getSubServices() {
        return subServices;
    }

    public void setSubServices(List<SubServices> subServices) {
        this.subServices = subServices;
    }

    @Override
    public String toString() {
        return "ServiceInformation{" +
                "serviceName='" + serviceName + '\'' +
                ", serviceCode='" + serviceCode + '\'' +
                ", subServices=" + subServices +
                '}';
    }
}
