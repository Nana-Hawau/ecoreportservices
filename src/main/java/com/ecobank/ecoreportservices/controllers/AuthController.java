package com.ecobank.ecoreportservices.controllers;

import com.ecobank.ecoreportservices.models.ResponseAuth;
import com.ecobank.ecoreportservices.services.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;



@RestController
@RequestMapping("/api/v1")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class AuthController {

    @Autowired
    AuthService authService;

    @GetMapping(value = "/token")
    public ResponseEntity<ResponseAuth> getToken(final HttpServletRequest httpServletRequest) throws Exception {
        String clientId = httpServletRequest.getHeader("x-client-id");
        String clientSecret = httpServletRequest.getHeader("x-client-secret");
        String sourceCode = httpServletRequest.getHeader("x-source-code");

        return authService.getAccessToken(clientId, clientSecret, sourceCode);
    }




}
