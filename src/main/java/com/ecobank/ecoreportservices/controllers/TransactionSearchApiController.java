package com.ecobank.ecoreportservices.controllers;

import com.ecobank.ecoreportservices.models.*;
import com.ecobank.ecoreportservices.services.*;
import com.ecobank.ecoreportservices.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;


@RestController
@RequestMapping("/api/v1")
public class TransactionSearchApiController {

    @Autowired
    ServiceInformationFactory serviceInformationFactory;

    @Autowired
    TransactionListService transactionListService;

//    @Autowired
//    TransactionDetailsService transactionDetailsService;

    @Autowired
    AffiliateService affiliateService;

//    @Autowired
//    AccountInfoEnquiryService accountInfoEnquiryService;

    @Autowired
    SecurityUtils securityUtils;

    @Autowired
    MergeDetailsService mergeDetailsService;


    @GetMapping(value = "/service-information-lookup")
    public ResponseEntity<?> serviceInformation(final HttpServletRequest httpServletRequest) throws Exception {
        AesKeyPair keys = securityUtils.extractKeys(httpServletRequest);
        return serviceInformationFactory.build(keys);
    }


    @PostMapping(value = "/transactions")
    public ResponseEntity<?> transactionList(@RequestBody String transactionList, final HttpServletRequest httpServletRequest) throws Exception {
        AesKeyPair keys = securityUtils.extractKeys(httpServletRequest);
        String encryptedAesKey = httpServletRequest.getHeader("x-secret-key");

        TransactionList transactionListRequest = securityUtils.decryptRequest(transactionList, keys, encryptedAesKey, TransactionList.class);
        return transactionListService.checkForServicesAndCallServiceMethod(transactionListRequest, keys);
    }


    @GetMapping(value = "/affiliates")
    public ResponseEntity<?> getAffiliateList(final HttpServletRequest httpServletRequest) throws Exception {
        AesKeyPair keys = securityUtils.extractKeys(httpServletRequest);
        return affiliateService.affiliate(keys);
    }


//    @PostMapping(value = "/account-information")
//    public ResponseEntity<?> getAccountInfo(@RequestBody String accountEnquiry, final HttpServletRequest httpServletRequest) throws Exception {
//        String encryptedAesKey = httpServletRequest.getHeader("x-secret-key");
//        AesKeyPair keys = securityUtils.extractKeys(httpServletRequest);
//        AccountEnquiry accountEnquiryRequest = securityUtils.decryptRequest(accountEnquiry, keys, encryptedAesKey, AccountEnquiry.class);
//        return accountInfoEnquiryService.getAccountInfo(accountEnquiryRequest, keys);
//    }


    @PostMapping(value = "/transaction-detail")
    public ResponseEntity<?> transactionDetail(@RequestBody String transactionDetailsRequest, final HttpServletRequest httpServletRequest) throws Exception {
        String encryptedAesKey = httpServletRequest.getHeader("x-secret-key");
        AesKeyPair keys = securityUtils.extractKeys(httpServletRequest);
        TransactionDetailsRequest detailsRequest = securityUtils.decryptRequest(transactionDetailsRequest, keys, encryptedAesKey, TransactionDetailsRequest.class);
        return mergeDetailsService.mergeDetails(detailsRequest, keys);
    }


//    @PostMapping(value = "/mapped-errors")
//    public ResponseEntity<?> mappedErrors(@RequestBody String mappedErrorRequest, final HttpServletRequest httpServletRequest) throws Exception {
//        String encryptedAesKey = httpServletRequest.getHeader("x-secret-key");
//        AesKeyPair keys = securityUtils.extractKeys(httpServletRequest);
//
//        MappedErrorRequest mappedErrorReq = securityUtils.decryptRequest(mappedErrorRequest, keys, encryptedAesKey, MappedErrorRequest.class);
//
//        return transactionDetailsService.mappedErrors(mappedErrorReq, keys);
//
//    }


}
