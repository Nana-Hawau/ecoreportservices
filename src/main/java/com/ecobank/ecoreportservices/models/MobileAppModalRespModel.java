package com.ecobank.ecoreportservices.models;

import java.math.BigDecimal;

public class MobileAppModalRespModel {
    private String transactionRef;
    private String transactionDateTime;
    private String customerId;
    private String customerName;
    private String customerAccNo;
    private String tranCcy;
    private BigDecimal amount;
    private String beneficiaryAccNo;
    private String destinationBankCode;
    private String responseCode;
    private String responseDesc;
    private String proposedResolution;
    private String productCode;
    private String debitAccount;
    private String billerCode;
    private String sessionId;
    private String customnerPhoneNumber;

    public String getTransactionRef() {
        return transactionRef;
    }

    public void setTransactionRef(String transactionRef) {
        this.transactionRef = transactionRef;
    }

    public String getTransactionDateTime() {
        return transactionDateTime;
    }

    public void setTransactionDateTime(String transactionDateTime) {
        this.transactionDateTime = transactionDateTime;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerAccNo() {
        return customerAccNo;
    }

    public void setCustomerAccNo(String customerAccNo) {
        this.customerAccNo = customerAccNo;
    }

    public String getTranCcy() {
        return tranCcy;
    }

    public void setTranCcy(String tranCcy) {
        this.tranCcy = tranCcy;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getBeneficiaryAccNo() {
        return beneficiaryAccNo;
    }

    public void setBeneficiaryAccNo(String beneficiaryAccNo) {
        this.beneficiaryAccNo = beneficiaryAccNo;
    }

    public String getDestinationBankCode() {
        return destinationBankCode;
    }

    public void setDestinationBankCode(String destinationBankCode) {
        this.destinationBankCode = destinationBankCode;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseDesc() {
        return responseDesc;
    }

    public void setResponseDesc(String responseDesc) {
        this.responseDesc = responseDesc;
    }

    public String getProposedResolution() {
        return proposedResolution;
    }

    public void setProposedResolution(String proposedResolution) {
        this.proposedResolution = proposedResolution;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getDebitAccount() {
        return debitAccount;
    }

    public void setDebitAccount(String debitAccount) {
        this.debitAccount = debitAccount;
    }

    public String getBillerCode() {
        return billerCode;
    }

    public void setBillerCode(String billerCode) {
        this.billerCode = billerCode;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getCustomnerPhoneNumber() {
        return customnerPhoneNumber;
    }

    public void setCustomnerPhoneNumber(String customnerPhoneNumber) {
        this.customnerPhoneNumber = customnerPhoneNumber;
    }

    @Override
    public String toString() {
        return "MobileAppModalRespModel{" +
                "transactionRef='" + transactionRef + '\'' +
                ", transactionDateTime='" + transactionDateTime + '\'' +
                ", customerId='" + customerId + '\'' +
                ", customerName='" + customerName + '\'' +
                ", customerAccNo='" + customerAccNo + '\'' +
                ", tranCcy='" + tranCcy + '\'' +
                ", amount=" + amount +
                ", beneficiaryAccNo='" + beneficiaryAccNo + '\'' +
                ", destinationBankCode='" + destinationBankCode + '\'' +
                ", responseCode='" + responseCode + '\'' +
                ", responseDesc='" + responseDesc + '\'' +
                ", proposedResolution='" + proposedResolution + '\'' +
                ", productCode='" + productCode + '\'' +
                ", debitAccount='" + debitAccount + '\'' +
                ", billerCode='" + billerCode + '\'' +
                ", sessionId='" + sessionId + '\'' +
                ", customnerPhoneNumber='" + customnerPhoneNumber + '\'' +
                '}';
    }
}
