package com.ecobank.ecoreportservices.models;

import java.math.BigDecimal;

public class CardsSearchDataRespModel {

    private String accountName;
    private String cardAccount;
    private String transactionRefNumber;
    private BigDecimal amount;
    private String currency;
    private String transactionDate;
    private String terminalId;
    private String returnReferenceNumber;
    private String solId;
    private String atmOwner;
    private String typeOfTransaction;
    private String atmTransactionType;
    private String stan;
    private String pan;
    private String transactionStatus;
    private String errorCode;
    private String errorMessage;
    private String region;

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getCardAccount() {
        return cardAccount;
    }

    public void setCardAccount(String cardAccount) {
        this.cardAccount = cardAccount;
    }

    public String getTransactionRefNumber() {
        return transactionRefNumber;
    }

    public void setTransactionRefNumber(String transactionRefNumber) {
        this.transactionRefNumber = transactionRefNumber;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getReturnReferenceNumber() {
        return returnReferenceNumber;
    }

    public void setReturnReferenceNumber(String returnReferenceNumber) {
        this.returnReferenceNumber = returnReferenceNumber;
    }

    public String getSolId() {
        return solId;
    }

    public void setSolId(String solId) {
        this.solId = solId;
    }

    public String getAtmOwner() {
        return atmOwner;
    }

    public void setAtmOwner(String atmOwner) {
        this.atmOwner = atmOwner;
    }

    public String getTypeOfTransaction() {
        return typeOfTransaction;
    }

    public void setTypeOfTransaction(String typeOfTransaction) {
        this.typeOfTransaction = typeOfTransaction;
    }

    public String getAtmTransactionType() {
        return atmTransactionType;
    }

    public void setAtmTransactionType(String atmTransactionType) {
        this.atmTransactionType = atmTransactionType;
    }

    public String getStan() {
        return stan;
    }

    public void setStan(String stan) {
        this.stan = stan;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    @Override
    public String toString() {
        return "CardsSearchDataRespModel{" +
                "accountName='" + accountName + '\'' +
                ", cardAccount='" + cardAccount + '\'' +
                ", transactionRefNumber='" + transactionRefNumber + '\'' +
                ", amount=" + amount +
                ", currency='" + currency + '\'' +
                ", transactionDate='" + transactionDate + '\'' +
                ", terminalId='" + terminalId + '\'' +
                ", returnReferenceNumber='" + returnReferenceNumber + '\'' +
                ", solId='" + solId + '\'' +
                ", atmOwner='" + atmOwner + '\'' +
                ", typeOfTransaction='" + typeOfTransaction + '\'' +
                ", atmTransactionType='" + atmTransactionType + '\'' +
                ", stan='" + stan + '\'' +
                ", pan='" + pan + '\'' +
                ", transactionStatus='" + transactionStatus + '\'' +
                ", errorCode='" + errorCode + '\'' +
                ", errorMessage='" + errorMessage + '\'' +
                ", region='" + region + '\'' +
                '}';
    }
}
