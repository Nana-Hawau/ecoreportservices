package com.ecobank.ecoreportservices.models;


import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseHeader {
    private String sourceCode;
    private String requestId;
    private String affiliateCode;
    private String requestToken;
    private String requestType;
    private String responseCode;
    private String responseMessage;
    private String ipAddress;
    private String sourceChannelId;
    private String transactionReference;
    private String fulfilmentReference;
    private String queuedTransaction;


    public String getSourceCode() {
        return sourceCode;
    }

    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getAffiliateCode() {
        return affiliateCode;
    }

    public void setAffiliateCode(String affiliateCode) {
        this.affiliateCode = affiliateCode;
    }

    public String getRequestToken() {
        return requestToken;
    }

    public void setRequestToken(String requestToken) {
        this.requestToken = requestToken;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getSourceChannelId() {
        return sourceChannelId;
    }

    public void setSourceChannelId(String sourceChannelId) {
        this.sourceChannelId = sourceChannelId;
    }

    public String getTransactionReference() {
        return transactionReference;
    }

    public void setTransactionReference(String transactionReference) {
        this.transactionReference = transactionReference;
    }

    public String getFulfilmentReference() {
        return fulfilmentReference;
    }

    public void setFulfilmentReference(String fulfilmentReference) {
        this.fulfilmentReference = fulfilmentReference;
    }

    public String getQueuedTransaction() {
        return queuedTransaction;
    }

    public void setQueuedTransaction(String queuedTransaction) {
        this.queuedTransaction = queuedTransaction;
    }

    @Override
    public String toString() {
        return "ResponseHeader{" +
                "sourceCode='" + sourceCode + '\'' +
                ", requestId='" + requestId + '\'' +
                ", affiliateCode='" + affiliateCode + '\'' +
                ", requestToken='" + requestToken + '\'' +
                ", requestType='" + requestType + '\'' +
                ", responseCode='" + responseCode + '\'' +
                ", responseMessage='" + responseMessage + '\'' +
                ", ipAddress='" + ipAddress + '\'' +
                ", sourceChannelId='" + sourceChannelId + '\'' +
                ", transactionReference='" + transactionReference + '\'' +
                ", fulfilmentReference='" + fulfilmentReference + '\'' +
                ", queuedTransaction='" + queuedTransaction + '\'' +
                '}';
    }
}
