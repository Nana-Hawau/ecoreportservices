package com.ecobank.ecoreportservices.models;

public class EcobankOnlineTransRequest {

    private String referenceNumber;
    private String startDate;
    private String endDate;
    private String sourceAcc;
    private String serviceName;

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getSourceAcc() {
        return sourceAcc;
    }

    public void setSourceAcc(String sourceAcc) {
        this.sourceAcc = sourceAcc;
    }

    @Override
    public String toString() {
        return "EcobankOnlineTransRequest{" +
                "referenceNumber='" + referenceNumber + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", sourceAcc='" + sourceAcc + '\'' +
                ", serviceName='" + serviceName + '\'' +
                '}';
    }
}
