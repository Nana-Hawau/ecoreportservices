package com.ecobank.ecoreportservices.models;

public class CardsSearchModalDataRespModel {

    private String errorCode;
    private String errorDescription;
    private String proposedResolution;
    private String errorCategory;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public String getProposedResolution() {
        return proposedResolution;
    }

    public void setProposedResolution(String proposedResolution) {
        this.proposedResolution = proposedResolution;
    }

    public String getErrorCategory() {
        return errorCategory;
    }

    public void setErrorCategory(String errorCategory) {
        this.errorCategory = errorCategory;
    }

    @Override
    public String toString() {
        return "CardsSearchModalDataRespModel{" +
                "errorCode='" + errorCode + '\'' +
                ", errorDescription='" + errorDescription + '\'' +
                ", proposedResolution='" + proposedResolution + '\'' +
                ", errorCategory='" + errorCategory + '\'' +
                '}';
    }
}
