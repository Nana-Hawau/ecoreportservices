package com.ecobank.ecoreportservices.models;

import lombok.Data;

@Data
public class AccountDetails {
    public String accountName;
    public String email;
    public String phoneNumber;
    public String customerCategory;

    public AccountDetails() {
    }

    public AccountDetails(String accountName, String email, String phoneNumber, String customerCategory) {
        this.accountName = accountName;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.customerCategory = customerCategory;
    }
}
