package com.ecobank.ecoreportservices.models;

public class MappedErrorRespModel {
    private String errorCode;
    private String errorDesc;
    private String proposedResolution;
    private String serviceName;
    private String errorCategory;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDesc() {
        return errorDesc;
    }

    public void setErrorDesc(String errorDesc) {
        this.errorDesc = errorDesc;
    }

    public String getProposedResolution() {
        return proposedResolution;
    }

    public void setProposedResolution(String proposedResolution) {
        this.proposedResolution = proposedResolution;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getErrorCategory() {
        return errorCategory;
    }

    public void setErrorCategory(String errorCategory) {
        this.errorCategory = errorCategory;
    }

    @Override
    public String toString() {
        return "MappedErrorRespModel{" +
                "errorCode='" + errorCode + '\'' +
                ", errorDesc='" + errorDesc + '\'' +
                ", proposedResolution='" + proposedResolution + '\'' +
                ", serviceName='" + serviceName + '\'' +
                ", errorCategory='" + errorCategory + '\'' +
                '}';
    }
}
