package com.ecobank.ecoreportservices.models;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class MappedErrorRequest {
        @NotEmpty(message = "Sub Service Code must be present")
    private String subServiceCode;
    @NotEmpty(message = "Error Code must be present")
    private String errorCode;

}
