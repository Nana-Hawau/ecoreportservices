package com.ecobank.ecoreportservices.models;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class TransactionList {
    @NotEmpty(message = "Service Code must be present")
    private String serviceCode;
    @NotEmpty(message = "Sub Service Code must be present")
    private String subServiceCode;
    @NotEmpty(message = "Start date must be present")
    private String startDate;
    @NotEmpty(message = "End date must be present")
    private String endDate;
    @NotNull(message = "Record Limit Must be present")
    private int recordLimit;
    @NotEmpty(message = "Source Account Number must be present")
    private String sourceAccount;
    private String beneficiaryAccount;
    private String affiliate;
    private String transactionReference;
    private BigDecimal amount;


}
