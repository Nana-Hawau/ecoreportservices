package com.ecobank.ecoreportservices.models;

public class MobileAppRespModel {
    private String transactionRef;
    private String tranDateTime;
    private String customerAccNo;
    private String respomseCode;
    private String errorCategory;
    private String countryCode;
    private String requestId;
    private String responseDesc;
    private String amount;
    private String beneficiaryAccNo;
    private String sendCCy;
    private String destinationCcy;
    private String ccy;
    private String productCode;
    private String beneficiaryBank;

    public String getTransactionRef() {
        return transactionRef;
    }

    public void setTransactionRef(String transactionRef) {
        this.transactionRef = transactionRef;
    }

    public String getTranDateTime() {
        return tranDateTime;
    }

    public void setTranDateTime(String tranDateTime) {
        this.tranDateTime = tranDateTime;
    }

    public String getCustomerAccNo() {
        return customerAccNo;
    }

    public void setCustomerAccNo(String customerAccNo) {
        this.customerAccNo = customerAccNo;
    }


    public String getErrorCategory() {
        return errorCategory;
    }

    public void setErrorCategory(String errorCategory) {
        this.errorCategory = errorCategory;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getRespomseCode() {
        return respomseCode;
    }

    public void setRespomseCode(String respomseCode) {
        this.respomseCode = respomseCode;
    }

    public String getResponseDesc() {
        return responseDesc;
    }

    public void setResponseDesc(String responseDesc) {
        this.responseDesc = responseDesc;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getBeneficiaryAccNo() {
        return beneficiaryAccNo;
    }

    public void setBeneficiaryAccNo(String beneficiaryAccNo) {
        this.beneficiaryAccNo = beneficiaryAccNo;
    }

    public String getSendCCy() {
        return sendCCy;
    }

    public void setSendCCy(String sendCCy) {
        this.sendCCy = sendCCy;
    }

    public String getDestinationCcy() {
        return destinationCcy;
    }

    public void setDestinationCcy(String destinationCcy) {
        this.destinationCcy = destinationCcy;
    }

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getBeneficiaryBank() {
        return beneficiaryBank;
    }

    public void setBeneficiaryBank(String beneficiaryBank) {
        this.beneficiaryBank = beneficiaryBank;
    }

    @Override
    public String toString() {
        return "MobileAppRespModel{" +
                "transactionRef='" + transactionRef + '\'' +
                ", tranDateTime='" + tranDateTime + '\'' +
                ", customerAccNo='" + customerAccNo + '\'' +
                ", respomseCode='" + respomseCode + '\'' +
                ", errorCategory='" + errorCategory + '\'' +
                ", countryCode='" + countryCode + '\'' +
                ", requestId='" + requestId + '\'' +
                ", responseDesc='" + responseDesc + '\'' +
                ", amount='" + amount + '\'' +
                ", beneficiaryAccNo='" + beneficiaryAccNo + '\'' +
                ", sendCCy='" + sendCCy + '\'' +
                ", destinationCcy='" + destinationCcy + '\'' +
                ", ccy='" + ccy + '\'' +
                ", productCode='" + productCode + '\'' +
                ", beneficiaryBank='" + beneficiaryBank + '\'' +
                '}';
    }
}
