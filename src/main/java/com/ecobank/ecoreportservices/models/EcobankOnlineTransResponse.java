package com.ecobank.ecoreportservices.models;

public class EcobankOnlineTransResponse {
    private String name;
    private String accountNumber;
    private String startDate;
    private String endDate;
    private String reference;
    private String country;
    private String serviceName;
    private String billerId;
    private String userId;
    private String requestId;
    private String requestToken;
    private String channel;
    private String billerCode;
    private String billerRefNo;
    private String cbaPostingRequired;
    private String customerRefNo;
    private String productCode;
    private String amount;
    private String currency;
    private String paymentDesciption;
    private String sourceCode;
    private String responseCode;
    private String responseMsg;
    private String requestType;
    private String affiliateCode;
    private String sourceAccount;
    private String customerName;
    private String paymentDate;
    private String transactionReferenceNumber;
    private String narration;
    private String sendingBankCode;
    private  String destinationBankCode;
    private  String beneficiaryAccountNo;
    private String transferNo;
    private String transactionId;
    private  String paymentReferenceNo;
    private  String txnccy;
    private String msgid;
    private String branch;
    private String remrk;
    private String cpacno;
    private String counterprtyname;
    private String actamt;
    private String authorizationstatus;
    private String entity;
    private  String txnamt;
    private String bankname;
    private String destinationCountry;
    private  String actdt;
    private String catgpurptyp;
    private  String cusacccy;
    private String cusavlbal;

    public String getCatgpurptyp() {
        return catgpurptyp;
    }

    public void setCatgpurptyp(String catgpurptyp) {
        this.catgpurptyp = catgpurptyp;
    }

    public String getCusacccy() {
        return cusacccy;
    }

    public void setCusacccy(String cusacccy) {
        this.cusacccy = cusacccy;
    }

    public String getCusavlbal() {
        return cusavlbal;
    }

    public void setCusavlbal(String cusavlbal) {
        this.cusavlbal = cusavlbal;
    }

    public String getCusaclcf() {
        return cusaclcf;
    }

    public void setCusaclcf(String cusaclcf) {
        this.cusaclcf = cusaclcf;
    }

    public String getAcdesc() {
        return acdesc;
    }

    public void setAcdesc(String acdesc) {
        this.acdesc = acdesc;
    }

    private  String cusaclcf;
    private String acdesc;

    public String getActdt() {
        return actdt;
    }

    public void setActdt(String actdt) {
        this.actdt = actdt;
    }

    public String getDestinationCountry() {
        return destinationCountry;
    }

    public void setDestinationCountry(String destinationCountry) {
        this.destinationCountry = destinationCountry;
    }

    public String getBeneficiaryname() {
        return beneficiaryname;
    }

    public void setBeneficiaryname(String beneficiaryname) {
        this.beneficiaryname = beneficiaryname;
    }

    private String beneficiaryname;









//


    public String getTransactionReferenceNumber() {
        return transactionReferenceNumber;
    }

    public void setTransactionReferenceNumber(String transactionReferenceNumber) {
        this.transactionReferenceNumber = transactionReferenceNumber;
    }

    public String getNarration() {
        return narration;
    }

    public void setNarration(String narration) {
        this.narration = narration;
    }

    public String getSendingBankCode() {
        return sendingBankCode;
    }

    public void setSendingBankCode(String sendingBankCode) {
        this.sendingBankCode = sendingBankCode;
    }

    public String getDestinationBankCode() {
        return destinationBankCode;
    }

    public void setDestinationBankCode(String destinationBankCode) {
        this.destinationBankCode = destinationBankCode;
    }

    public String getBeneficiaryAccountNo() {
        return beneficiaryAccountNo;
    }

    public void setBeneficiaryAccountNo(String beneficiaryAccountNo) {
        this.beneficiaryAccountNo = beneficiaryAccountNo;
    }

    public String getTransferNo() {
        return transferNo;
    }

    public void setTransferNo(String transferNo) {
        this.transferNo = transferNo;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getPaymentReferenceNo() {
        return paymentReferenceNo;
    }

    public void setPaymentReferenceNo(String paymentReferenceNo) {
        this.paymentReferenceNo = paymentReferenceNo;
    }

    public String getTxnccy() {
        return txnccy;
    }

    public void setTxnccy(String txnccy) {
        this.txnccy = txnccy;
    }

    public String getMsgid() {
        return msgid;
    }

    public void setMsgid(String msgid) {
        this.msgid = msgid;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getRemrk() {
        return remrk;
    }

    public void setRemrk(String remrk) {
        this.remrk = remrk;
    }

    public String getCpacno() {
        return cpacno;
    }

    public void setCpacno(String cpacno) {
        this.cpacno = cpacno;
    }

    public String getCounterprtyname() {
        return counterprtyname;
    }

    public void setCounterprtyname(String counterprtyname) {
        this.counterprtyname = counterprtyname;
    }

    public String getActamt() {
        return actamt;
    }

    public void setActamt(String actamt) {
        this.actamt = actamt;
    }

    public String getAuthorizationstatus() {
        return authorizationstatus;
    }

    public void setAuthorizationstatus(String authorizationstatus) {
        this.authorizationstatus = authorizationstatus;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public String getTxnamt() {
        return txnamt;
    }

    public void setTxnamt(String txnamt) {
        this.txnamt = txnamt;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getBillerId() {
        return billerId;
    }

    public void setBillerId(String billerId) {
        this.billerId = billerId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getRequestToken() {
        return requestToken;
    }

    public void setRequestToken(String requestToken) {
        this.requestToken = requestToken;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getBillerCode() {
        return billerCode;
    }

    public void setBillerCode(String billerCode) {
        this.billerCode = billerCode;
    }

    public String getBillerRefNo() {
        return billerRefNo;
    }

    public void setBillerRefNo(String billerRefNo) {
        this.billerRefNo = billerRefNo;
    }

    public String getCbaPostingRequired() {
        return cbaPostingRequired;
    }

    public void setCbaPostingRequired(String cbaPostingRequired) {
        this.cbaPostingRequired = cbaPostingRequired;
    }

    public String getCustomerRefNo() {
        return customerRefNo;
    }

    public void setCustomerRefNo(String customerRefNo) {
        this.customerRefNo = customerRefNo;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getPaymentDesciption() {
        return paymentDesciption;
    }

    public void setPaymentDesciption(String paymentDesciption) {
        this.paymentDesciption = paymentDesciption;
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getAffiliateCode() {
        return affiliateCode;
    }

    public void setAffiliateCode(String affiliateCode) {
        this.affiliateCode = affiliateCode;
    }

    public String getSourceAccount() {
        return sourceAccount;
    }

    public void setSourceAccount(String sourceAccount) {
        this.sourceAccount = sourceAccount;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    @Override
    public String toString() {
        return "EcobankOnlineTransResponse{" +
                "name='" + name + '\'' +
                ", accountNumber='" + accountNumber + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", reference='" + reference + '\'' +
                ", country='" + country + '\'' +
                ", serviceName='" + serviceName + '\'' +
                ", billerId='" + billerId + '\'' +
                ", userId='" + userId + '\'' +
                ", requestId='" + requestId + '\'' +
                ", requestToken='" + requestToken + '\'' +
                ", channel='" + channel + '\'' +
                ", billerCode='" + billerCode + '\'' +
                ", billerRefNo='" + billerRefNo + '\'' +
                ", cbaPostingRequired='" + cbaPostingRequired + '\'' +
                ", customerRefNo='" + customerRefNo + '\'' +
                ", productCode='" + productCode + '\'' +
                ", amount='" + amount + '\'' +
                ", currency='" + currency + '\'' +
                ", paymentDesciption='" + paymentDesciption + '\'' +
                ", sourceCode='" + sourceCode + '\'' +
                ", responseCode='" + responseCode + '\'' +
                ", responseMsg='" + responseMsg + '\'' +
                ", requestType='" + requestType + '\'' +
                ", affiliateCode='" + affiliateCode + '\'' +
                ", sourceAccount='" + sourceAccount + '\'' +
                ", customerName='" + customerName + '\'' +
                ", paymentDate='" + paymentDate + '\'' +
                ", transactionReferenceNumber='" + transactionReferenceNumber + '\'' +
                ", narration='" + narration + '\'' +
                ", sendingBankCode='" + sendingBankCode + '\'' +
                ", destinationBankCode='" + destinationBankCode + '\'' +
                ", beneficiaryAccountNo='" + beneficiaryAccountNo + '\'' +
                ", transferNo='" + transferNo + '\'' +
                ", transactionId='" + transactionId + '\'' +
                ", paymentReferenceNo='" + paymentReferenceNo + '\'' +
                ", txnccy='" + txnccy + '\'' +
                ", msgid='" + msgid + '\'' +
                ", branch='" + branch + '\'' +
                ", remrk='" + remrk + '\'' +
                ", cpacno='" + cpacno + '\'' +
                ", counterprtyname='" + counterprtyname + '\'' +
                ", actamt='" + actamt + '\'' +
                ", authorizationstatus='" + authorizationstatus + '\'' +
                ", entity='" + entity + '\'' +
                ", txnamt='" + txnamt + '\'' +
                ", bankname='" + bankname + '\'' +
                ", destinationCountry='" + destinationCountry + '\'' +
                ", actdt='" + actdt + '\'' +
                ", catgpurptyp='" + catgpurptyp + '\'' +
                ", cusacccy='" + cusacccy + '\'' +
                ", cusavlbal='" + cusavlbal + '\'' +
                ", cusaclcf='" + cusaclcf + '\'' +
                ", acdesc='" + acdesc + '\'' +
                ", beneficiaryname='" + beneficiaryname + '\'' +
                '}';
    }
}

