package com.ecobank.ecoreportservices.models;

import java.util.List;

public class Response {
    private String code;
    private List<Object> data;
    private String message;

    public Response () {
    }

    public Response(String code, String message, List<Object> data) {
        this.code = code;
        this.message = message;
        this.data = data;

    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<Object> getData() {
        return data;
    }

    public void setData(List<Object> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
