package com.ecobank.ecoreportservices.models;

import org.json.JSONObject;

public class UDFData {
    private String udfValue;

    private String udfDataType;

    private String udfName;

    private String udfCode;

    public UDFData(Object udfData) {
    }

    public String getUdfValue() {
        return udfValue;
    }

    public void setUdfValue(String udfValue) {
        this.udfValue = udfValue;
    }

    public String getUdfDataType() {
        return udfDataType;
    }

    public void setUdfDataType(String udfDataType) {
        this.udfDataType = udfDataType;
    }

    public String getUdfName() {
        return udfName;
    }

    public void setUdfName(String udfName) {
        this.udfName = udfName;
    }

    public String getUdfCode() {
        return udfCode;
    }

    public void setUdfCode(String udfCode) {
        this.udfCode = udfCode;
    }

    @Override
    public String toString() {
        return "UDFData{" +
                "udfValue='" + udfValue + '\'' +
                ", udfDataType='" + udfDataType + '\'' +
                ", udfName='" + udfName + '\'' +
                ", udfCode='" + udfCode + '\'' +
                '}';
    }
}
