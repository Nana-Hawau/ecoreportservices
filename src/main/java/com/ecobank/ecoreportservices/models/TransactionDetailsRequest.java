package com.ecobank.ecoreportservices.models;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class TransactionDetailsRequest {
    @NotEmpty(message = "Sub Service Code must be present")
    private String subServiceCode;

    @NotEmpty(message = "Transaction Reference must be present")
    private String transactionReference;

    @NotEmpty(message = "Error Code must be present")
    private String errorCode;

    @NotEmpty(message = "Account Number Code must be present")
    private String accountNumber;

    @NotEmpty(message = "Affiliate Code must be present")
    private String affiliate;



}
