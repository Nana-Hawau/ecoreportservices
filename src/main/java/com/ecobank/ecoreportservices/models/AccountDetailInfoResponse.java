package com.ecobank.ecoreportservices.models;

import java.util.Arrays;

public class AccountDetailInfoResponse {
    private String affiliateCode;
    private String accountNo;
    private String accountName;
    private String currency;
    private String branchCode;
    private String availableBalance;
    private String currentBalance;
    private String accountType;
    private String accountStatus;
    private String accountClass;
    private String ccy;
    private String customerID;
    private String bvn;
    private ResponseHeader responseHeader;
    private String emailID;
    private String identificationType;
    private String blockedAmount;
    private String phoneNo;
    private String ODLimit;
    private String customerType;
    private String dob;
    private UDFData[] UDFData;
    private String identificationNo;


    public String getAffiliateCode() {
        return affiliateCode;
    }

    public void setAffiliateCode(String affiliateCode) {
        this.affiliateCode = affiliateCode;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(String availableBalance) {
        this.availableBalance = availableBalance;
    }

    public String getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(String currentBalance) {
        this.currentBalance = currentBalance;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    public String getAccountClass() {
        return accountClass;
    }

    public void setAccountClass(String accountClass) {
        this.accountClass = accountClass;
    }

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public String getBvn() {
        return bvn;
    }

    public void setBvn(String bvn) {
        this.bvn = bvn;
    }

    public ResponseHeader getResponseHeader() {
        return responseHeader;
    }

    public void setResponseHeader(ResponseHeader responseHeader) {
        this.responseHeader = responseHeader;
    }

    public String getEmailID() {
        return emailID;
    }

    public void setEmailID(String emailID) {
        this.emailID = emailID;
    }

    public String getIdentificationType() {
        return identificationType;
    }

    public void setIdentificationType(String identificationType) {
        this.identificationType = identificationType;
    }

    public String getBlockedAmount() {
        return blockedAmount;
    }

    public void setBlockedAmount(String blockedAmount) {
        this.blockedAmount = blockedAmount;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getODLimit() {
        return ODLimit;
    }

    public void setODLimit(String ODLimit) {
        this.ODLimit = ODLimit;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public UDFData[] getUDFData() {
        return UDFData;
    }

    public void setUDFData(UDFData[] UDFData) {
        this.UDFData = UDFData;
    }

    public String getIdentificationNo() {
        return identificationNo;
    }

    public void setIdentificationNo(String identificationNo) {
        this.identificationNo = identificationNo;
    }

    @Override
    public String toString() {
        return "AccountDetailInfoResponse{" +
                "affiliateCode='" + affiliateCode + '\'' +
                ", accountNo='" + accountNo + '\'' +
                ", accountName='" + accountName + '\'' +
                ", currency='" + currency + '\'' +
                ", branchCode='" + branchCode + '\'' +
                ", availableBalance='" + availableBalance + '\'' +
                ", currentBalance='" + currentBalance + '\'' +
                ", accountType='" + accountType + '\'' +
                ", accountStatus='" + accountStatus + '\'' +
                ", accountClass='" + accountClass + '\'' +
                ", ccy='" + ccy + '\'' +
                ", customerID='" + customerID + '\'' +
                ", bvn='" + bvn + '\'' +
                ", responseHeader=" + responseHeader +
                ", emailID='" + emailID + '\'' +
                ", identificationType='" + identificationType + '\'' +
                ", blockedAmount='" + blockedAmount + '\'' +
                ", phoneNo='" + phoneNo + '\'' +
                ", ODLimit='" + ODLimit + '\'' +
                ", customerType='" + customerType + '\'' +
                ", dob='" + dob + '\'' +
                ", UDFData=" + Arrays.toString(UDFData) +
                ", identificationNo='" + identificationNo + '\'' +
                '}';
    }
}
