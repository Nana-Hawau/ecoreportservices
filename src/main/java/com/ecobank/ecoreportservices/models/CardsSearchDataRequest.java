package com.ecobank.ecoreportservices.models;

import java.math.BigDecimal;

public class CardsSearchDataRequest {

    private String cluster;
    private String startDate;
    private String endDate;
    private Integer recordLimit;
    private String cardAccount;
    private BigDecimal amount;
    private String transactionRefNumber;

    public String getCluster() {
        return cluster;
    }

    public void setCluster(String cluster) {
        this.cluster = cluster;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Integer getRecordLimit() {
        return recordLimit;
    }

    public void setRecordLimit(Integer recordLimit) {
        this.recordLimit = recordLimit;
    }

    public String getCardAccount() {
        return cardAccount;
    }

    public void setCardAccount(String cardAccount) {
        this.cardAccount = cardAccount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getTransactionRefNumber() {
        return transactionRefNumber;
    }

    public void setTransactionRefNumber(String transactionRefNumber) {
        this.transactionRefNumber = transactionRefNumber;
    }

    @Override
    public String toString() {
        return "CardsSearchDataRequest{" +
                "cluster='" + cluster + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", recordLimit=" + recordLimit +
                ", cardAccount='" + cardAccount + '\'' +
                ", amount=" + amount +
                ", transactionRefNumber='" + transactionRefNumber + '\'' +
                '}';
    }
}
