package com.ecobank.ecoreportservices.models;

public class AffiliateListModel {

    private String countryCode;
    private String country;

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "AffiliateListModel{" +
                "countryCode='" + countryCode + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}
