package com.ecobank.ecoreportservices.models;

public class AesKeyPair {

	private String publicKey;
	private String privateKey;
	private String secretKey;

	public AesKeyPair() {
	};

	public AesKeyPair(String publicKey, String privateKey) {
		super();
		this.publicKey = publicKey;
		this.privateKey = privateKey;
	}

	public String getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}

	public String getPrivateKey() {
		return privateKey;
	}

	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	@Override
	public String toString() {
		return "AesKeyPair [publicKey=" + publicKey + ", privateKey=" + privateKey + ", secretKey=" + secretKey + "]";
	}

}
