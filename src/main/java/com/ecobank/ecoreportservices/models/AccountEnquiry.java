package com.ecobank.ecoreportservices.models;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
@Data
public class AccountEnquiry {
    public AccountEnquiry() {
    }

    public AccountEnquiry(@NotEmpty(message = "Account Number cannot be empty") String accountNumber, @NotEmpty(message = "Affiliate Code must be present") String affiliate) {
        this.accountNumber = accountNumber;
        this.affiliate = affiliate;
    }

    @NotEmpty(message = "Account Number cannot be empty")
    private String accountNumber;
    @NotEmpty(message = "Affiliate Code must be present")
    private String affiliate;
}
