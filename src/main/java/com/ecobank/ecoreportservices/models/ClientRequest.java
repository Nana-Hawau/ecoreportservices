package com.ecobank.ecoreportservices.models;

import javax.validation.constraints.NotEmpty;

public class ClientRequest {

    @NotEmpty(message = "Client Id is empty")
    private String clientId;
    @NotEmpty(message = "Client Secret is empty")
    private String clientSecret;
    @NotEmpty(message = "Source Code is empty")
    private String sourceCode;


    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }
}
