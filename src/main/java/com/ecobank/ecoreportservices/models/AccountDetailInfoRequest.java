package com.ecobank.ecoreportservices.models;

public class AccountDetailInfoRequest {
   private HostHeaderInfo hostHeaderInfo;
   private String accountNo;

    public void setHostHeaderInfo(HostHeaderInfo hostHeaderInfo) {
        this.hostHeaderInfo = hostHeaderInfo;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    @Override
    public String toString() {
        return "AccountDetailInfoRequest{" +
                "hostHeaderInfo=" + hostHeaderInfo +
                ", accountNo='" + accountNo + '\'' +
                '}';
    }
}
