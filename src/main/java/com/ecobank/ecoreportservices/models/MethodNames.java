package com.ecobank.ecoreportservices.models;

public class MethodNames {
    private String serviceName;
    private String subServiceName;
    private String message;


    public MethodNames() {
    }

    public MethodNames(String serviceName, String subServiceName, String message) {
        this.serviceName = serviceName;
        this.subServiceName = subServiceName;
        this.message = message;
    }

    public MethodNames(String serviceName, String subServiceName) {
        this.serviceName = serviceName;
        this.subServiceName = subServiceName;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getSubServiceName() {
        return subServiceName;
    }

    public void setSubServiceName(String subServiceName) {
        this.subServiceName = subServiceName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "MethodNames{" +
                "serviceName='" + serviceName + '\'' +
                ", subServiceName=" + subServiceName +
                '}';
    }
}
